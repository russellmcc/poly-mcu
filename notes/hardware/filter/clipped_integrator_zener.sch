EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Opamp_Quad_Generic U1
U 1 1 5DA21222
P 2550 1350
F 0 "U1" H 2550 983 50  0000 C CNN
F 1 "Opamp_Quad_Generic" H 2550 1074 50  0000 C CNN
F 2 "" H 2550 1350 50  0001 C CNN
F 3 "~" H 2550 1350 50  0001 C CNN
F 4 "X" H 2550 1350 50  0001 C CNN "Spice_Primitive"
F 5 "OPAMP1" H 2550 1350 50  0001 C CNN "Spice_Model"
F 6 "Y" H 2550 1350 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "/Users/russruss/proj/poly_notes/notes/hardware/filter/ideal_opamp.mod" H 2550 1350 50  0001 C CNN "Spice_Lib_File"
F 8 "3 2 1" H 2550 1350 50  0001 C CNN "Spice_Node_Sequence"
	1    2550 1350
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5DA220EC
P 2200 1450
F 0 "#PWR02" H 2200 1200 50  0001 C CNN
F 1 "GND" V 2350 1500 50  0000 R CNN
F 2 "" H 2200 1450 50  0001 C CNN
F 3 "" H 2200 1450 50  0001 C CNN
	1    2200 1450
	0    1    1    0   
$EndComp
Wire Wire Line
	2200 1450 2250 1450
Wire Wire Line
	2250 1250 2050 1250
$Comp
L Device:C C1
U 1 1 5DA2252A
P 2450 1750
F 0 "C1" V 2300 1900 50  0000 C CNN
F 1 "180pF" V 2289 1750 50  0000 C CNN
F 2 "" H 2488 1600 50  0001 C CNN
F 3 "~" H 2450 1750 50  0001 C CNN
	1    2450 1750
	0    1    1    0   
$EndComp
Wire Wire Line
	2850 1350 2850 1750
Wire Wire Line
	2850 1750 2600 1750
Wire Wire Line
	2300 1750 2050 1750
Wire Wire Line
	2050 1750 2050 1250
Connection ~ 2050 1250
Wire Wire Line
	2050 1250 1950 1250
Connection ~ 2850 1750
Wire Wire Line
	2050 2000 2050 1750
Connection ~ 2050 1750
Text GLabel 2850 1550 2    50   Input ~ 0
V_OUT
$Comp
L pspice:ISOURCE I1
U 1 1 5DA23DDB
P 1550 1250
F 0 "I1" V 1035 1250 50  0000 C CNN
F 1 "sin(0 60uA 440)" V 1126 1250 50  0000 C CNN
F 2 "" H 1550 1250 50  0001 C CNN
F 3 "~" H 1550 1250 50  0001 C CNN
	1    1550 1250
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5DA251E8
P 1100 1250
F 0 "#PWR01" H 1100 1000 50  0001 C CNN
F 1 "GND" V 1250 1300 50  0000 R CNN
F 2 "" H 1100 1250 50  0001 C CNN
F 3 "" H 1100 1250 50  0001 C CNN
	1    1100 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	1100 1250 1150 1250
Text Notes 1250 2000 0    50   ~ 0
.tran 1us 10ms
$Comp
L Device:D D1
U 1 1 5DA26861
P 2250 2000
F 0 "D1" H 2250 2216 50  0000 C CNN
F 1 "D" H 2250 2125 50  0000 C CNN
F 2 "" H 2250 2000 50  0001 C CNN
F 3 "~" H 2250 2000 50  0001 C CNN
F 4 "X" H 2250 2000 50  0001 C CNN "Spice_Primitive"
F 5 "mm3z3v6t1g" H 2250 2000 50  0001 C CNN "Spice_Model"
F 6 "Y" H 2250 2000 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "/Users/russruss/proj/poly_notes/notes/hardware/filter/mm3z3v6t1g.mod" H 2250 2000 50  0001 C CNN "Spice_Lib_File"
	1    2250 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 2000 2100 2000
Wire Wire Line
	2850 1750 2850 2000
$Comp
L Device:D D2
U 1 1 5DA28C98
P 2650 2000
F 0 "D2" H 2650 1784 50  0000 C CNN
F 1 "D" H 2650 1875 50  0000 C CNN
F 2 "" H 2650 2000 50  0001 C CNN
F 3 "~" H 2650 2000 50  0001 C CNN
F 4 "X" H 2650 2000 50  0001 C CNN "Spice_Primitive"
F 5 "mm3z3v6t1g" H 2650 2000 50  0001 C CNN "Spice_Model"
F 6 "Y" H 2650 2000 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "/Users/russruss/proj/poly_notes/notes/hardware/filter/mm3z3v6t1g.mod" H 2650 2000 50  0001 C CNN "Spice_Lib_File"
	1    2650 2000
	-1   0    0    1   
$EndComp
Wire Wire Line
	2400 2000 2500 2000
Wire Wire Line
	2800 2000 2850 2000
$EndSCHEMATC
