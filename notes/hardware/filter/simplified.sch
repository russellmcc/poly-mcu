EESchema Schematic File Version 4
LIBS:simplified-cache
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L local:SSM2164 U2
U 2 1 5DA1E628
P 3300 2300
F 0 "U2" H 3350 2715 50  0000 C CNN
F 1 "SSM2164" H 3350 2624 50  0000 C CNN
F 2 "" H 3300 2300 50  0001 C CNN
F 3 "" H 3300 2300 50  0001 C CNN
F 4 "X" H 3300 2300 50  0001 C CNN "Spice_Primitive"
F 5 "SSM2164" H 3300 2300 50  0001 C CNN "Spice_Model"
F 6 "Y" H 3300 2300 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "2 4 3" H 3300 2300 50  0001 C CNN "Spice_Node_Sequence"
F 8 "/Users/russruss/proj/poly_notes/notes/hardware/ssm2164/ssm2164_basic.mod" H 3300 2300 50  0001 C CNN "Spice_Lib_File"
	2    3300 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 2750 3300 2900
Text GLabel 3300 2900 2    50   Input ~ 0
F_CV
$Comp
L power:GND #PWR06
U 1 1 5DA26ADE
P 4200 2500
F 0 "#PWR06" H 4200 2250 50  0001 C CNN
F 1 "GND" V 4205 2372 50  0000 R CNN
F 2 "" H 4200 2500 50  0001 C CNN
F 3 "" H 4200 2500 50  0001 C CNN
	1    4200 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	3700 2300 3950 2300
Wire Wire Line
	4200 2500 4250 2500
$Comp
L Device:C C1
U 1 1 5DA27164
P 4500 1950
F 0 "C1" V 4248 1950 50  0000 C CNN
F 1 "180pF" V 4339 1950 50  0000 C CNN
F 2 "" H 4538 1800 50  0001 C CNN
F 3 "~" H 4500 1950 50  0001 C CNN
	1    4500 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	4350 1950 3950 1950
Wire Wire Line
	3950 1950 3950 2300
Connection ~ 3950 2300
Wire Wire Line
	3950 2300 4250 2300
Wire Wire Line
	4650 1950 5100 1950
Wire Wire Line
	5100 1950 5100 2400
Wire Wire Line
	5100 2400 4850 2400
$Comp
L Device:R_US R7
U 1 1 5DA27D3D
P 5450 2400
F 0 "R7" V 5245 2400 50  0000 C CNN
F 1 "33k" V 5336 2400 50  0000 C CNN
F 2 "" V 5490 2390 50  0001 C CNN
F 3 "~" H 5450 2400 50  0001 C CNN
	1    5450 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 2400 5300 2400
Connection ~ 5100 2400
Wire Wire Line
	5600 2400 5750 2400
Text GLabel 6100 2900 2    50   Input ~ 0
F_CV
Wire Wire Line
	6050 2900 6100 2900
Wire Wire Line
	6050 2850 6050 2900
$Comp
L power:GND #PWR07
U 1 1 5DA2AE2F
P 6950 2600
F 0 "#PWR07" H 6950 2350 50  0001 C CNN
F 1 "GND" V 6955 2472 50  0000 R CNN
F 2 "" H 6950 2600 50  0001 C CNN
F 3 "" H 6950 2600 50  0001 C CNN
	1    6950 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	6450 2400 6700 2400
Wire Wire Line
	6950 2600 7000 2600
$Comp
L Device:C C2
U 1 1 5DA2AE37
P 7250 2050
F 0 "C2" V 6998 2050 50  0000 C CNN
F 1 "180pF" V 7089 2050 50  0000 C CNN
F 2 "" H 7288 1900 50  0001 C CNN
F 3 "~" H 7250 2050 50  0001 C CNN
	1    7250 2050
	0    1    1    0   
$EndComp
Wire Wire Line
	7100 2050 6700 2050
Wire Wire Line
	6700 2050 6700 2400
Connection ~ 6700 2400
Wire Wire Line
	6700 2400 7000 2400
Wire Wire Line
	7400 2050 7850 2050
Wire Wire Line
	7850 2050 7850 2500
Wire Wire Line
	7850 2500 7600 2500
Wire Wire Line
	7850 2500 8050 2500
Connection ~ 7850 2500
$Comp
L Device:Opamp_Quad_Generic U1
U 1 1 5DA36B7C
P 1800 2000
F 0 "U1" H 1800 1633 50  0000 C CNN
F 1 "Opamp_Quad_Generic" H 1800 1724 50  0000 C CNN
F 2 "" H 1800 2000 50  0001 C CNN
F 3 "~" H 1800 2000 50  0001 C CNN
F 4 "X" H 1800 2000 50  0001 C CNN "Spice_Primitive"
F 5 "OPAMP1" H 1800 2000 50  0001 C CNN "Spice_Model"
F 6 "Y" H 1800 2000 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "3 2 1" H 1800 2000 50  0001 C CNN "Spice_Node_Sequence"
F 8 "/Users/russruss/proj/poly_notes/notes/hardware/filter/ideal_opamp.mod" H 1800 2000 50  0001 C CNN "Spice_Lib_File"
	1    1800 2000
	1    0    0    1   
$EndComp
$Comp
L Device:R_US R4
U 1 1 5DA399DB
P 2550 2000
F 0 "R4" V 2345 2000 50  0000 C CNN
F 1 "33k" V 2436 2000 50  0000 C CNN
F 2 "" V 2590 1990 50  0001 C CNN
F 3 "~" H 2550 2000 50  0001 C CNN
	1    2550 2000
	0    1    1    0   
$EndComp
Wire Wire Line
	2100 2000 2250 2000
Wire Wire Line
	2700 2000 2950 2000
Wire Wire Line
	2950 2000 2950 2300
$Comp
L power:GND #PWR01
U 1 1 5DA3B615
P 1350 2100
F 0 "#PWR01" H 1350 1850 50  0001 C CNN
F 1 "GND" V 1450 2150 50  0000 R CNN
F 2 "" H 1350 2100 50  0001 C CNN
F 3 "" H 1350 2100 50  0001 C CNN
	1    1350 2100
	0    1    1    0   
$EndComp
Wire Wire Line
	1350 2100 1500 2100
$Comp
L Device:R_US R2
U 1 1 5DA3BE9E
P 1750 1500
F 0 "R2" V 1545 1500 50  0000 C CNN
F 1 "33k" V 1636 1500 50  0000 C CNN
F 2 "" V 1790 1490 50  0001 C CNN
F 3 "~" H 1750 1500 50  0001 C CNN
	1    1750 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	2250 2000 2250 1500
Wire Wire Line
	2250 1500 1900 1500
Connection ~ 2250 2000
Wire Wire Line
	2250 2000 2400 2000
Wire Wire Line
	1600 1500 1300 1500
Wire Wire Line
	1300 1500 1300 1900
Wire Wire Line
	1300 1900 1500 1900
$Comp
L Device:R_US R1
U 1 1 5DA45463
P 1750 1200
F 0 "R1" V 1545 1200 50  0000 C CNN
F 1 "33k" V 1636 1200 50  0000 C CNN
F 2 "" V 1790 1190 50  0001 C CNN
F 3 "~" H 1750 1200 50  0001 C CNN
	1    1750 1200
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 2500 8050 1200
Wire Wire Line
	8050 1200 1900 1200
Wire Wire Line
	1600 1200 1300 1200
Wire Wire Line
	1300 1200 1300 1500
Connection ~ 1300 1500
$Comp
L Device:R_US R6
U 1 1 5DA4696A
P 4800 3500
F 0 "R6" V 4595 3500 50  0000 C CNN
F 1 "33k" V 4686 3500 50  0000 C CNN
F 2 "" V 4840 3490 50  0001 C CNN
F 3 "~" H 4800 3500 50  0001 C CNN
	1    4800 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 2400 5100 3500
Wire Wire Line
	5100 3500 4950 3500
Wire Wire Line
	4650 3500 4550 3500
$Comp
L power:GND #PWR05
U 1 1 5DA5100A
P 3750 3700
F 0 "#PWR05" H 3750 3450 50  0001 C CNN
F 1 "GND" V 3755 3572 50  0000 R CNN
F 2 "" H 3750 3700 50  0001 C CNN
F 3 "" H 3750 3700 50  0001 C CNN
	1    3750 3700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3700 3700 3750 3700
Wire Wire Line
	3700 3500 3750 3500
$Comp
L Device:R_US R5
U 1 1 5DA564E1
P 3400 3150
F 0 "R5" V 3300 3000 50  0000 C CNN
F 1 "33k" V 3300 3150 50  0000 C CNN
F 2 "" V 3440 3140 50  0001 C CNN
F 3 "~" H 3400 3150 50  0001 C CNN
	1    3400 3150
	0    1    1    0   
$EndComp
Wire Wire Line
	3750 3500 3750 3150
Wire Wire Line
	3750 3150 3550 3150
Connection ~ 3750 3500
Wire Wire Line
	3750 3500 3850 3500
Wire Wire Line
	3250 3150 3000 3150
Wire Wire Line
	3000 3150 3000 3600
Wire Wire Line
	3000 3600 3100 3600
Text GLabel 4300 4000 2    50   Input ~ 0
Q_CV
Wire Wire Line
	4250 3950 4250 4000
Wire Wire Line
	4250 4000 4300 4000
$Comp
L Device:R_US R3
U 1 1 5DA59B83
P 1800 3600
F 0 "R3" V 1595 3600 50  0000 C CNN
F 1 "16.5k" V 1686 3600 50  0000 C CNN
F 2 "" V 1840 3590 50  0001 C CNN
F 3 "~" H 1800 3600 50  0001 C CNN
	1    1800 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 3600 3000 3600
Connection ~ 3000 3600
Wire Wire Line
	1650 3600 1200 3600
Wire Wire Line
	1200 3600 1200 1500
Wire Wire Line
	1200 1500 1300 1500
Wire Wire Line
	2950 2300 3000 2300
Wire Wire Line
	1200 1500 850  1500
Text GLabel 850  1500 0    50   Input ~ 0
I_IN
Text GLabel 8150 2500 2    50   Input ~ 0
V_LP
Wire Wire Line
	8050 2500 8150 2500
Connection ~ 8050 2500
$Comp
L pspice:VSOURCE V1
U 1 1 5DA6412B
P 1800 4500
F 0 "V1" H 2028 4546 50  0000 L CNN
F 1 "1.2" H 2028 4455 50  0000 L CNN
F 2 "" H 1800 4500 50  0001 C CNN
F 3 "~" H 1800 4500 50  0001 C CNN
	1    1800 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5DA650FB
P 1800 4850
F 0 "#PWR02" H 1800 4600 50  0001 C CNN
F 1 "GND" H 1805 4677 50  0000 C CNN
F 2 "" H 1800 4850 50  0001 C CNN
F 3 "" H 1800 4850 50  0001 C CNN
	1    1800 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 4800 1800 4850
Wire Wire Line
	1800 4200 1800 4100
Text GLabel 1800 4100 0    50   Input ~ 0
F_CV
$Comp
L pspice:VSOURCE V2
U 1 1 5DA681E1
P 2500 4500
F 0 "V2" H 2728 4546 50  0000 L CNN
F 1 "0" H 2728 4455 50  0000 L CNN
F 2 "" H 2500 4500 50  0001 C CNN
F 3 "~" H 2500 4500 50  0001 C CNN
	1    2500 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5DA681EB
P 2500 4850
F 0 "#PWR03" H 2500 4600 50  0001 C CNN
F 1 "GND" H 2505 4677 50  0000 C CNN
F 2 "" H 2500 4850 50  0001 C CNN
F 3 "" H 2500 4850 50  0001 C CNN
	1    2500 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 4800 2500 4850
Wire Wire Line
	2500 4200 2500 4100
Text GLabel 2500 4100 0    50   Input ~ 0
Q_CV
$Comp
L power:GND #PWR04
U 1 1 5DA6AA91
P 3350 4950
F 0 "#PWR04" H 3350 4700 50  0001 C CNN
F 1 "GND" H 3355 4777 50  0000 C CNN
F 2 "" H 3350 4950 50  0001 C CNN
F 3 "" H 3350 4950 50  0001 C CNN
	1    3350 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 4900 3350 4950
Wire Wire Line
	3350 4100 3350 4000
Text GLabel 3350 4000 0    50   Input ~ 0
I_IN
Connection ~ 1200 1500
Text Notes 4750 4300 0    50   ~ 0
* .tran 1uS 300mS\n.ac oct 20 1 20k
$Comp
L Device:Opamp_Quad_Generic U3
U 1 1 5DA712F6
P 3400 3600
F 0 "U3" H 3400 3233 50  0000 C CNN
F 1 "Opamp_Quad_Generic" H 3400 3324 50  0000 C CNN
F 2 "" H 3400 3600 50  0001 C CNN
F 3 "~" H 3400 3600 50  0001 C CNN
F 4 "X" H 3400 3600 50  0001 C CNN "Spice_Primitive"
F 5 "OPAMP1" H 3400 3600 50  0001 C CNN "Spice_Model"
F 6 "Y" H 3400 3600 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "3 2 1" H 3400 3600 50  0001 C CNN "Spice_Node_Sequence"
F 8 "/Users/russruss/proj/poly_notes/notes/hardware/filter/ideal_opamp.mod" H 3400 3600 50  0001 C CNN "Spice_Lib_File"
	1    3400 3600
	-1   0    0    1   
$EndComp
$Comp
L Device:Opamp_Quad_Generic U5
U 1 1 5DA756BB
P 4550 2400
F 0 "U5" H 4550 2033 50  0000 C CNN
F 1 "Opamp_Quad_Generic" H 4550 2124 50  0000 C CNN
F 2 "" H 4550 2400 50  0001 C CNN
F 3 "~" H 4550 2400 50  0001 C CNN
F 4 "X" H 4550 2400 50  0001 C CNN "Spice_Primitive"
F 5 "OPAMP1" H 4550 2400 50  0001 C CNN "Spice_Model"
F 6 "Y" H 4550 2400 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "3 2 1" H 4550 2400 50  0001 C CNN "Spice_Node_Sequence"
F 8 "/Users/russruss/proj/poly_notes/notes/hardware/filter/ideal_opamp.mod" H 4550 2400 50  0001 C CNN "Spice_Lib_File"
	1    4550 2400
	1    0    0    1   
$EndComp
$Comp
L Device:Opamp_Quad_Generic U7
U 1 1 5DA7664E
P 7300 2500
F 0 "U7" H 7300 2133 50  0000 C CNN
F 1 "Opamp_Quad_Generic" H 7300 2224 50  0000 C CNN
F 2 "" H 7300 2500 50  0001 C CNN
F 3 "~" H 7300 2500 50  0001 C CNN
F 4 "X" H 7300 2500 50  0001 C CNN "Spice_Primitive"
F 5 "OPAMP1" H 7300 2500 50  0001 C CNN "Spice_Model"
F 6 "Y" H 7300 2500 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "3 2 1" H 7300 2500 50  0001 C CNN "Spice_Node_Sequence"
F 8 "/Users/russruss/proj/poly_notes/notes/hardware/filter/ideal_opamp.mod" H 7300 2500 50  0001 C CNN "Spice_Lib_File"
	1    7300 2500
	1    0    0    1   
$EndComp
$Comp
L local:SSM2164 U6
U 2 1 5DA7A364
P 6050 2400
F 0 "U6" H 6100 2815 50  0000 C CNN
F 1 "SSM2164" H 6100 2724 50  0000 C CNN
F 2 "" H 6050 2400 50  0001 C CNN
F 3 "" H 6050 2400 50  0001 C CNN
F 4 "X" H 6050 2400 50  0001 C CNN "Spice_Primitive"
F 5 "SSM2164" H 6050 2400 50  0001 C CNN "Spice_Model"
F 6 "Y" H 6050 2400 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "2 4 3" H 6050 2400 50  0001 C CNN "Spice_Node_Sequence"
F 8 "/Users/russruss/proj/poly_notes/notes/hardware/ssm2164/ssm2164_basic.mod" H 6050 2400 50  0001 C CNN "Spice_Lib_File"
	2    6050 2400
	1    0    0    -1  
$EndComp
$Comp
L local:SSM2164 U4
U 2 1 5DA7C25F
P 4250 3500
F 0 "U4" H 4300 3915 50  0000 C CNN
F 1 "SSM2164" H 4300 3824 50  0000 C CNN
F 2 "" H 4250 3500 50  0001 C CNN
F 3 "" H 4250 3500 50  0001 C CNN
F 4 "X" H 4250 3500 50  0001 C CNN "Spice_Primitive"
F 5 "SSM2164" H 4250 3500 50  0001 C CNN "Spice_Model"
F 6 "Y" H 4250 3500 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "2 4 3" H 4250 3500 50  0001 C CNN "Spice_Node_Sequence"
F 8 "/Users/russruss/proj/poly_notes/notes/hardware/ssm2164/ssm2164_basic.mod" H 4250 3500 50  0001 C CNN "Spice_Lib_File"
	2    4250 3500
	-1   0    0    -1  
$EndComp
$Comp
L pspice:ISOURCE I1
U 1 1 5DA6A647
P 3350 4500
F 0 "I1" H 3580 4546 50  0000 L CNN
F 1 "DC sin(0 5uA 50) AC 30uA" H 3580 4455 50  0000 L CNN
F 2 "" H 3350 4500 50  0001 C CNN
F 3 "~" H 3350 4500 50  0001 C CNN
	1    3350 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 1500 5100 1950
$Comp
L Device:D D2
U 1 1 5DA455DA
P 4650 1500
F 0 "D2" H 4650 1716 50  0000 C CNN
F 1 "D" H 4650 1625 50  0000 C CNN
F 2 "" H 4650 1500 50  0001 C CNN
F 3 "~" H 4650 1500 50  0001 C CNN
F 4 "X" H 4650 1500 50  0001 C CNN "Spice_Primitive"
F 5 "mm3z3v6t1g" H 4650 1500 50  0001 C CNN "Spice_Model"
F 6 "Y" H 4650 1500 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "/Users/russruss/proj/poly_notes/notes/hardware/filter/mm3z3v6t1g.mod" H 4650 1500 50  0001 C CNN "Spice_Lib_File"
	1    4650 1500
	-1   0    0    1   
$EndComp
Wire Wire Line
	5100 1500 4800 1500
Wire Wire Line
	3950 1950 3950 1500
$Comp
L Device:D D1
U 1 1 5DA455E6
P 4250 1500
F 0 "D1" H 4250 1284 50  0000 C CNN
F 1 "D" H 4250 1375 50  0000 C CNN
F 2 "" H 4250 1500 50  0001 C CNN
F 3 "~" H 4250 1500 50  0001 C CNN
F 4 "X" H 4250 1500 50  0001 C CNN "Spice_Primitive"
F 5 "mm3z3v6t1g" H 4250 1500 50  0001 C CNN "Spice_Model"
F 6 "Y" H 4250 1500 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "/Users/russruss/proj/poly_notes/notes/hardware/filter/mm3z3v6t1g.mod" H 4250 1500 50  0001 C CNN "Spice_Lib_File"
	1    4250 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 1500 4400 1500
Wire Wire Line
	4100 1500 3950 1500
Connection ~ 3950 1950
Connection ~ 5100 1950
$EndSCHEMATC
