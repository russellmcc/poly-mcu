* manually adapted from Red LED - https://www.mouser.com/datasheet/2/180/IN-S85AT_Series_V1.0-1488297.pdf
.model Dled_test D (Is=1e-19 Rs=.85 N=2)