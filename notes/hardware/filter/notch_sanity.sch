EESchema Schematic File Version 4
LIBS:notch_sanity-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR01
U 1 1 5E6E9C9B
P 4050 2850
F 0 "#PWR01" H 4050 2600 50  0001 C CNN
F 1 "GND" H 4055 2677 50  0000 C CNN
F 2 "" H 4050 2850 50  0001 C CNN
F 3 "" H 4050 2850 50  0001 C CNN
	1    4050 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 2850 4050 2800
$Comp
L Device:R_US R1
U 1 1 5E6EA726
P 4400 2000
F 0 "R1" H 4468 2046 50  0000 L CNN
F 1 "5k" H 4468 1955 50  0000 L CNN
F 2 "" V 4440 1990 50  0001 C CNN
F 3 "~" H 4400 2000 50  0001 C CNN
	1    4400 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 2200 4400 2200
Wire Wire Line
	4400 2200 4400 2150
$Comp
L Device:R_US R2
U 1 1 5E6EB060
P 4750 2000
F 0 "R2" H 4818 2046 50  0000 L CNN
F 1 "5k" H 4818 1955 50  0000 L CNN
F 2 "" V 4790 1990 50  0001 C CNN
F 3 "~" H 4750 2000 50  0001 C CNN
	1    4750 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5E6EB807
P 5000 2900
F 0 "#PWR02" H 5000 2650 50  0001 C CNN
F 1 "GND" H 5005 2727 50  0000 C CNN
F 2 "" H 5000 2900 50  0001 C CNN
F 3 "" H 5000 2900 50  0001 C CNN
	1    5000 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2900 5000 2850
Wire Wire Line
	4750 2150 5000 2150
Wire Wire Line
	5000 2150 5000 2250
Wire Wire Line
	4750 1850 4750 1750
Wire Wire Line
	4750 1750 4550 1750
Wire Wire Line
	4400 1750 4400 1850
Wire Wire Line
	4550 1750 4550 1550
Wire Wire Line
	4550 1550 4900 1550
Connection ~ 4550 1750
Wire Wire Line
	4550 1750 4400 1750
$Comp
L Device:R_US R3
U 1 1 5E6EC0B4
P 5050 1550
F 0 "R3" V 4845 1550 50  0000 C CNN
F 1 "33k" V 4936 1550 50  0000 C CNN
F 2 "" V 5090 1540 50  0001 C CNN
F 3 "~" H 5050 1550 50  0001 C CNN
	1    5050 1550
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5E6EC82B
P 5250 1550
F 0 "#PWR03" H 5250 1300 50  0001 C CNN
F 1 "GND" H 5255 1377 50  0000 C CNN
F 2 "" H 5250 1550 50  0001 C CNN
F 3 "" H 5250 1550 50  0001 C CNN
	1    5250 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 1550 5250 1550
Text Notes 5400 2250 0    50   ~ 0
.tran 10uS 30mS\n
$Comp
L pspice:VSOURCE V2
U 1 1 5E6E9DBC
P 5000 2550
F 0 "V2" H 5228 2596 50  0000 L CNN
F 1 "sin(0 2 321)" H 5228 2505 50  0000 L CNN
F 2 "" H 5000 2550 50  0001 C CNN
F 3 "~" H 5000 2550 50  0001 C CNN
	1    5000 2550
	1    0    0    -1  
$EndComp
$Comp
L pspice:VSOURCE V1
U 1 1 5E6E912B
P 4050 2500
F 0 "V1" H 4278 2546 50  0000 L CNN
F 1 "sin(0 2 45)" H 4278 2455 50  0000 L CNN
F 2 "" H 4050 2500 50  0001 C CNN
F 3 "~" H 4050 2500 50  0001 C CNN
	1    4050 2500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
