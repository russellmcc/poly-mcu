EESchema Schematic File Version 4
LIBS:clipped_integrator_led-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Opamp_Quad_Generic U1
U 1 1 5DA24098
P 2350 1300
F 0 "U1" H 2350 933 50  0000 C CNN
F 1 "Opamp_Quad_Generic" H 2350 1024 50  0000 C CNN
F 2 "" H 2350 1300 50  0001 C CNN
F 3 "~" H 2350 1300 50  0001 C CNN
F 4 "X" H 2350 1300 50  0001 C CNN "Spice_Primitive"
F 5 "OPAMP1" H 2350 1300 50  0001 C CNN "Spice_Model"
F 6 "Y" H 2350 1300 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "/Users/russruss/proj/poly_notes/notes/hardware/filter/ideal_opamp.mod" H 2350 1300 50  0001 C CNN "Spice_Lib_File"
F 8 "3 2 1" H 2350 1300 50  0001 C CNN "Spice_Node_Sequence"
	1    2350 1300
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5DA2409E
P 2000 1400
F 0 "#PWR02" H 2000 1150 50  0001 C CNN
F 1 "GND" V 2150 1450 50  0000 R CNN
F 2 "" H 2000 1400 50  0001 C CNN
F 3 "" H 2000 1400 50  0001 C CNN
	1    2000 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	2000 1400 2050 1400
Wire Wire Line
	2050 1200 1850 1200
$Comp
L Device:C C1
U 1 1 5DA240A6
P 2250 1700
F 0 "C1" V 2100 1850 50  0000 C CNN
F 1 "180pF" V 2089 1700 50  0000 C CNN
F 2 "" H 2288 1550 50  0001 C CNN
F 3 "~" H 2250 1700 50  0001 C CNN
	1    2250 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 1300 2650 1700
Wire Wire Line
	2650 1700 2400 1700
Wire Wire Line
	2100 1700 1850 1700
Wire Wire Line
	1850 1700 1850 1200
Connection ~ 1850 1200
Wire Wire Line
	1850 1200 1750 1200
Connection ~ 2650 1700
Wire Wire Line
	1850 1950 1850 1700
Connection ~ 1850 1700
Text GLabel 2650 1500 2    50   Input ~ 0
V_OUT
$Comp
L pspice:ISOURCE I1
U 1 1 5DA240B6
P 1350 1200
F 0 "I1" V 835 1200 50  0000 C CNN
F 1 "sin(0 60uA 440)" V 926 1200 50  0000 C CNN
F 2 "" H 1350 1200 50  0001 C CNN
F 3 "~" H 1350 1200 50  0001 C CNN
	1    1350 1200
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5DA240BC
P 900 1200
F 0 "#PWR01" H 900 950 50  0001 C CNN
F 1 "GND" V 1050 1250 50  0000 R CNN
F 2 "" H 900 1200 50  0001 C CNN
F 3 "" H 900 1200 50  0001 C CNN
	1    900  1200
	0    1    1    0   
$EndComp
Wire Wire Line
	900  1200 950  1200
Text Notes 1050 1950 0    50   ~ 0
.tran 1us 10ms
Wire Wire Line
	2650 1700 2650 1950
$Comp
L Device:LED D1
U 1 1 5DA24B9E
P 2250 1950
F 0 "D1" H 2150 2000 50  0000 R CNN
F 1 "LED" H 2450 2000 50  0000 R CNN
F 2 "" H 2250 1950 50  0001 C CNN
F 3 "~" H 2250 1950 50  0001 C CNN
F 4 "D" H 2250 1950 50  0001 C CNN "Spice_Primitive"
F 5 "Dled_test" H 2250 1950 50  0001 C CNN "Spice_Model"
F 6 "Y" H 2250 1950 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "led.mod" H 2250 1950 50  0001 C CNN "Spice_Lib_File"
F 8 "2 1" H 2250 1950 50  0001 C CNN "Spice_Node_Sequence"
	1    2250 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 1950 2650 1950
Wire Wire Line
	2100 1950 1850 1950
$Comp
L Device:LED D2
U 1 1 5DA25921
P 2250 2300
F 0 "D2" H 2400 2250 50  0000 R CNN
F 1 "LED" H 2450 2350 50  0000 R CNN
F 2 "" H 2250 2300 50  0001 C CNN
F 3 "~" H 2250 2300 50  0001 C CNN
F 4 "D" H 2250 2300 50  0001 C CNN "Spice_Primitive"
F 5 "Dled_test" H 2250 2300 50  0001 C CNN "Spice_Model"
F 6 "Y" H 2250 2300 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "led.mod" H 2250 2300 50  0001 C CNN "Spice_Lib_File"
F 8 "2 1" H 2250 2300 50  0001 C CNN "Spice_Node_Sequence"
	1    2250 2300
	-1   0    0    1   
$EndComp
Wire Wire Line
	1850 1950 1850 2300
Wire Wire Line
	1850 2300 2100 2300
Connection ~ 1850 1950
Wire Wire Line
	2400 2300 2650 2300
Wire Wire Line
	2650 2300 2650 1950
Connection ~ 2650 1950
$EndSCHEMATC
