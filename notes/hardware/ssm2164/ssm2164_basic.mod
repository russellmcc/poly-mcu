* SSM2164 VCA cell - emilie from mutable via https://www.muffwiggler.com/forum/viewtopic.php?t=155122&sid=20e0829964c50dd5c7008fe92b5aad9f
.SUBCKT SSM2164 IN OUT CV
* The two transistors in the input's differential pair have the same base
* current, hence the same Vbe. The second transistor is referenced to
* ground so IN is virtually grounded.
V1 IN 0 0
* Models 5k impedance for the CV input
R2 CV 0 5k
* Stability network
R3 IN 1 560
C1 1 0 560p
* Current gain expression
G1 OUT 0 VALUE={-I(V1)*PWR(10, -V(CV) * 1.5)}
.ENDS