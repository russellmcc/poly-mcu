EESchema Schematic File Version 4
LIBS:diode_clip-cache
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pspice:VSOURCE V1
U 1 1 5DB65F9E
P 3900 2200
F 0 "V1" H 4128 2246 50  0000 L CNN
F 1 "12" H 4128 2155 50  0000 L CNN
F 2 "" H 3900 2200 50  0001 C CNN
F 3 "~" H 3900 2200 50  0001 C CNN
	1    3900 2200
	1    0    0    -1  
$EndComp
$Comp
L pspice:VSOURCE V2
U 1 1 5DB66A6E
P 4750 2200
F 0 "V2" H 4978 2246 50  0000 L CNN
F 1 "-5" H 4978 2155 50  0000 L CNN
F 2 "" H 4750 2200 50  0001 C CNN
F 3 "~" H 4750 2200 50  0001 C CNN
	1    4750 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5DB670DF
P 3900 2550
F 0 "#PWR05" H 3900 2300 50  0001 C CNN
F 1 "GND" H 3905 2377 50  0000 C CNN
F 2 "" H 3900 2550 50  0001 C CNN
F 3 "" H 3900 2550 50  0001 C CNN
	1    3900 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 2500 3900 2550
$Comp
L power:GND #PWR07
U 1 1 5DB679F7
P 4750 2550
F 0 "#PWR07" H 4750 2300 50  0001 C CNN
F 1 "GND" H 4755 2377 50  0000 C CNN
F 2 "" H 4750 2550 50  0001 C CNN
F 3 "" H 4750 2550 50  0001 C CNN
	1    4750 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 2500 4750 2550
$Comp
L power:+12V #PWR04
U 1 1 5DB6803E
P 3900 1850
F 0 "#PWR04" H 3900 1700 50  0001 C CNN
F 1 "+12V" H 3915 2023 50  0000 C CNN
F 2 "" H 3900 1850 50  0001 C CNN
F 3 "" H 3900 1850 50  0001 C CNN
	1    3900 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1850 3900 1900
$Comp
L power:-5V #PWR06
U 1 1 5DB68989
P 4750 1850
F 0 "#PWR06" H 4750 1950 50  0001 C CNN
F 1 "-5V" H 4765 2023 50  0000 C CNN
F 2 "" H 4750 1850 50  0001 C CNN
F 3 "" H 4750 1850 50  0001 C CNN
	1    4750 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 1850 4750 1900
Text Notes 850  900  0    50   ~ 0
.tran 10u 3m 
Wire Wire Line
	2300 2200 2500 2200
Wire Wire Line
	2300 1600 2300 2200
Wire Wire Line
	2800 2200 2950 2200
Wire Wire Line
	2950 1800 2950 2200
$Comp
L Device:R_US R1
U 1 1 5DB63184
P 2650 2200
F 0 "R1" V 2550 2100 50  0000 C CNN
F 1 "270k" V 2550 2350 50  0000 C CNN
F 2 "" V 2690 2190 50  0001 C CNN
F 3 "~" H 2650 2200 50  0001 C CNN
	1    2650 2200
	0    1    1    0   
$EndComp
$Comp
L pspice:ISOURCE I1
U 1 1 5DB5FA80
P 1850 1600
F 0 "I1" V 1335 1600 50  0000 C CNN
F 1 "pulse(-7u 7u 0 1m 1m 1e-10 2m)" V 1426 1600 50  0000 C CNN
F 2 "" H 1850 1600 50  0001 C CNN
F 3 "~" H 1850 1600 50  0001 C CNN
	1    1850 1600
	0    1    1    0   
$EndComp
Text GLabel 3050 1800 2    50   Input ~ 0
V_OUT
Wire Wire Line
	2950 1800 3050 1800
Connection ~ 2950 1800
Wire Wire Line
	2950 1500 2950 1800
Wire Wire Line
	2300 1600 2250 1600
Wire Wire Line
	2550 1800 2550 1850
$Comp
L power:-5V #PWR03
U 1 1 5DB6277D
P 2550 1850
F 0 "#PWR03" H 2550 1950 50  0001 C CNN
F 1 "-5V" H 2450 1950 50  0000 C CNN
F 2 "" H 2550 1850 50  0001 C CNN
F 3 "" H 2550 1850 50  0001 C CNN
	1    2550 1850
	-1   0    0    1   
$EndComp
Wire Wire Line
	2550 1150 2550 1200
$Comp
L power:+12V #PWR02
U 1 1 5DB621E6
P 2550 1150
F 0 "#PWR02" H 2550 1000 50  0001 C CNN
F 1 "+12V" H 2565 1323 50  0000 C CNN
F 2 "" H 2550 1150 50  0001 C CNN
F 3 "" H 2550 1150 50  0001 C CNN
	1    2550 1150
	1    0    0    -1  
$EndComp
Connection ~ 2300 1600
Wire Wire Line
	2350 1600 2300 1600
Wire Wire Line
	2350 1100 2350 1400
$Comp
L power:GND #PWR01
U 1 1 5DB618D1
P 2350 1100
F 0 "#PWR01" H 2350 850 50  0001 C CNN
F 1 "GND" H 2355 927 50  0000 C CNN
F 2 "" H 2350 1100 50  0001 C CNN
F 3 "" H 2350 1100 50  0001 C CNN
	1    2350 1100
	-1   0    0    1   
$EndComp
$Comp
L pspice:OPAMP U1
U 1 1 5DB60824
P 2650 1500
F 0 "U1" H 2994 1546 50  0000 L CNN
F 1 "OPAMP" H 2994 1455 50  0000 L CNN
F 2 "" H 2650 1500 50  0001 C CNN
F 3 "~" H 2650 1500 50  0001 C CNN
F 4 "X" H 2650 1500 50  0001 C CNN "Spice_Primitive"
F 5 "LF356/NS" H 2650 1500 50  0001 C CNN "Spice_Model"
F 6 "Y" H 2650 1500 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "LF356.MOD" H 2650 1500 50  0001 C CNN "Spice_Lib_File"
F 8 "1 2 4 5 3" H 2650 1500 50  0001 C CNN "Spice_Node_Sequence"
	1    2650 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:D D1
U 1 1 5DB97378
P 2650 2350
F 0 "D1" H 2550 2400 50  0000 C CNN
F 1 "D" H 2750 2400 50  0000 C CNN
F 2 "" H 2650 2350 50  0001 C CNN
F 3 "~" H 2650 2350 50  0001 C CNN
F 4 "D" H 2650 2350 50  0001 C CNN "Spice_Primitive"
F 5 "LED1" H 2650 2350 50  0001 C CNN "Spice_Model"
F 6 "Y" H 2650 2350 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "leds.mod" H 2650 2350 50  0001 C CNN "Spice_Lib_File"
	1    2650 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 2200 2300 2350
Wire Wire Line
	2300 2350 2500 2350
Connection ~ 2300 2200
Wire Wire Line
	2800 2350 2950 2350
Wire Wire Line
	2950 2350 2950 2200
Connection ~ 2950 2200
$Comp
L Device:D D2
U 1 1 5DB97C31
P 2650 2500
F 0 "D2" H 2750 2450 50  0000 C CNN
F 1 "D" H 2550 2450 50  0000 C CNN
F 2 "" H 2650 2500 50  0001 C CNN
F 3 "~" H 2650 2500 50  0001 C CNN
F 4 "D" H 2650 2500 50  0001 C CNN "Spice_Primitive"
F 5 "LED1" H 2650 2500 50  0001 C CNN "Spice_Model"
F 6 "Y" H 2650 2500 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "leds.mod" H 2650 2500 50  0001 C CNN "Spice_Lib_File"
	1    2650 2500
	-1   0    0    1   
$EndComp
Wire Wire Line
	2800 2500 2950 2500
Wire Wire Line
	2950 2500 2950 2350
Connection ~ 2950 2350
Wire Wire Line
	2500 2500 2300 2500
Wire Wire Line
	2300 2500 2300 2350
Connection ~ 2300 2350
$EndSCHEMATC
