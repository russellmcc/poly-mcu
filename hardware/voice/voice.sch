EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2750 1250 950  650 
U 5DAB9CF3
F0 "OscCore" 50
F1 "OscCore.sch" 50
F2 "dac[0..12]" I L 2750 1750 50 
F3 "gpio[0..16]" I L 2750 1850 50 
F4 "OUT" O R 3700 1300 50 
$EndSheet
$Sheet
S 3950 1250 950  650 
U 5DA53E0F
F0 "Filter" 50
F1 "Filter.sch" 50
F2 "IN" I L 3950 1300 50 
F3 "OUT" O R 4900 1300 50 
F4 "dac[0..12]" I L 3950 1750 50 
F5 "gpio[0..16]" I L 3950 1850 50 
F6 "TUNING" O R 4900 1400 50 
$EndSheet
Wire Bus Line
	2650 1750 2650 2050
Wire Bus Line
	2650 2050 3850 2050
Wire Bus Line
	3850 2050 3850 1750
Wire Bus Line
	2650 1750 2750 1750
Wire Bus Line
	3850 1750 3950 1750
Wire Bus Line
	2750 1850 2700 1850
Wire Bus Line
	2700 1850 2700 2100
Wire Bus Line
	2700 2100 3900 2100
Wire Bus Line
	3900 2100 3900 1850
Wire Bus Line
	3900 1850 3950 1850
Wire Wire Line
	3700 1300 3950 1300
$Sheet
S 1550 1250 950  650 
U 5DE16C34
F0 "stm32" 50
F1 "stm32.sch" 50
F2 "dac[0..12]" O R 2500 1750 50 
F3 "gpio[0..16]" O R 2500 1850 50 
F4 "TUNING" I L 1550 1850 50 
F5 "SIGNAL" I L 1550 1750 50 
$EndSheet
Wire Bus Line
	2500 1750 2650 1750
Connection ~ 2650 1750
Wire Bus Line
	2500 1850 2700 1850
Connection ~ 2700 1850
Wire Wire Line
	4900 1400 5000 1400
Wire Wire Line
	5000 1400 5000 2200
Wire Wire Line
	5000 2200 1450 2200
Wire Wire Line
	1450 2200 1450 1850
Wire Wire Line
	1450 1850 1550 1850
Wire Wire Line
	1550 1750 1400 1750
Wire Wire Line
	1400 1750 1400 2250
Wire Wire Line
	1400 2250 5050 2250
Wire Wire Line
	5050 2250 5050 1300
Wire Wire Line
	5050 1300 4900 1300
$EndSCHEMATC
