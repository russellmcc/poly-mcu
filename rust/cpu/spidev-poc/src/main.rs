extern crate spidev;
use spidev::{SpiModeFlags, Spidev, SpidevOptions};
use std::io;
use std::io::prelude::*;

fn create_spi() -> io::Result<Spidev> {
    let mut spi = Spidev::open("/dev/spidev0.0")?;
    spi.configure(
        &SpidevOptions::new()
            .bits_per_word(8)
            .max_speed_hz(1_000)
            .mode(SpiModeFlags::SPI_MODE_0),
    )?;
    Ok(spi)
}

fn main() -> io::Result<()> {
    let stdin = io::stdin();
    let mut spi = create_spi()?;
    for line in stdin.lock().lines() {
        let contents = line?;
        println!("{}", &contents);
        write!(spi, "frequency is: {}\r\n", &contents)?;
    }
    Ok(())
}
