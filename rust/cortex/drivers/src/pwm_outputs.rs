use core::convert::Infallible;
use gpio::gpioc::{PC0, PC1, PC2, PC3, PC6, PC7, PC8, PC9};
use gpio::{AF2, AF4};
use hal::dac_array::DacArray;
use pac::{TIM1, TIM8};
use rcc::{Clocks, APB2};
use stm32f3xx_hal::{gpio, pac, rcc};
use unwrap_infallible::UnwrapInfallible;

pub trait Output1<T> {}
pub trait Output2<T> {}
pub trait Output3<T> {}
pub trait Output4<T> {}

impl<Otype> Output1<TIM1> for PC0<AF2<Otype>> {}
impl<Otype> Output2<TIM1> for PC1<AF2<Otype>> {}
impl<Otype> Output3<TIM1> for PC2<AF2<Otype>> {}
impl<Otype> Output4<TIM1> for PC3<AF2<Otype>> {}

impl<Otype> Output1<TIM8> for PC6<AF4<Otype>> {}
impl<Otype> Output2<TIM8> for PC7<AF4<Otype>> {}
impl<Otype> Output3<TIM8> for PC8<AF4<Otype>> {}
impl<Otype> Output4<TIM8> for PC9<AF4<Otype>> {}

pub struct Outputs<O1, O2, O3, O4> {
    pub o1: O1,
    pub o2: O2,
    pub o3: O3,
    pub o4: O4,
}

pub struct PWMOutputs<T, O1, O2, O3, O4> {
    tim: T,
    outputs: Outputs<O1, O2, O3, O4>,
}

fn underlying_enr(_apb2: &mut APB2) -> &stm32f3xx_hal::pac::rcc::APB2ENR {
    unsafe { &(*stm32f3xx_hal::pac::RCC::ptr()).apb2enr }
}

// Note that we set this to 11 bits so that if the pll is 144MHz, the timer frequency is ~70k
const NUM_PWM_BITS: u8 = 11;

macro_rules! pwm {
    ($($TIMX:ident: ($timXen:ident, $timXsw:ident),)+) => {
        $(
            impl<O1, O2, O3, O4> PWMOutputs<$TIMX, O1, O2, O3, O4> where
                O1: Output1<$TIMX>,
                O2: Output2<$TIMX>,
                O3: Output3<$TIMX>,
                O4: Output4<$TIMX> {
                pub fn new(tim: $TIMX,
                           apb2: &mut APB2,
                           outputs:Outputs<O1, O2, O3, O4>,
                           _clocks: Clocks) -> PWMOutputs<$TIMX, O1, O2, O3, O4> {
                    underlying_enr(apb2).modify(|_, w| w.$timXen().enabled());

                    debug_assert!(unsafe {
                        &(*stm32f3xx_hal::pac::RCC::ptr())
                    }.cr.read().pllon().is_on());

                    unsafe {
                        &(*stm32f3xx_hal::pac::RCC::ptr())
                    }.cfgr3.modify(|_, w| {w.$timXsw().pll()});

                    tim.cr1.write(|w| {w});
                    tim.psc.write(|w| {w});
                    tim.ccmr1_output().write(|w| {
                        w.oc1m().pwm_mode1()
                         .oc2m().pwm_mode1()
                    });
                    tim.ccmr2_output().write(|w| {
                        w.oc3m().pwm_mode1()
                         .oc4m().pwm_mode1()
                    });
                    tim.bdtr.modify(|_, w| {w.moe().enabled()});
                    tim.ccer.write(|w| w
                                   .cc1e().set_bit()
                                   .cc2e().set_bit()
                                   .cc3e().set_bit()
                                   .cc4e().set_bit());
                    tim.arr.write(|w| {w.arr().bits(1 << NUM_PWM_BITS)});

                    tim.cr1.modify(|_, w| {w.cen().enabled()});
                    let mut ret = PWMOutputs { tim, outputs };
                    for p in 0..3 {
                        ret.write(0, p).unwrap_infallible();
                    }
                    ret
                }

                pub fn free(self) -> ($TIMX, Outputs<O1, O2, O3, O4>) {
                    (self.tim, self.outputs)
                }
            }

            impl<O1, O2, O3, O4> DacArray<u16, u8> for PWMOutputs<$TIMX, O1, O2, O3, O4> where
                O1: Output1<$TIMX>,
                O2: Output2<$TIMX>,
                O3: Output3<$TIMX>,
                O4: Output4<$TIMX> {
                type Error = Infallible;
                fn write(&mut self, val: u16, port: u8) -> Result<(), Self::Error> {
                    assert!(port < 4);
                    match port {
                        0 => &self.tim.ccr1,
                        1 => &self.tim.ccr2,
                        2 => &self.tim.ccr3,
                        3 => &self.tim.ccr4,
                        _ => unreachable!()
                    }.write(|w| w.ccr().bits(val >> (16 - NUM_PWM_BITS)));
                    Ok(())
                }
            }
        )+}
}

pwm! {
    TIM1: (tim1en, tim1sw),
    TIM8: (tim8en, tim8sw),
}
