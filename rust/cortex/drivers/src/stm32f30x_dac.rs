use core::convert::Infallible;
use stm32f3xx_hal::gpio::gpioa::PA4;
use stm32f3xx_hal::gpio::Analog;
use stm32f3xx_hal::rcc::APB1;

fn underlying_enr(_apb1: &mut APB1) -> &stm32f3xx_hal::pac::rcc::APB1ENR {
    unsafe { &(*stm32f3xx_hal::pac::RCC::ptr()).apb1enr }
}

pub struct DAC {
    output: PA4<Analog>,
    dac: stm32f3xx_hal::pac::DAC1,
}

impl DAC {
    pub fn new(pa4: PA4<Analog>, dac: stm32f3xx_hal::pac::DAC1, apb1: &mut APB1) -> DAC {
        underlying_enr(apb1).modify(|_, w| w.dac1en().enabled());
        dac.cr.write(|w| w.en1().enabled().boff1().enabled());
        DAC { output: pa4, dac }
    }

    pub fn free(self, apb1: &mut APB1) -> (PA4<Analog>, stm32f3xx_hal::pac::DAC1) {
        self.dac.cr.write(|w| w);
        underlying_enr(apb1).modify(|_, w| w.dac1en().disabled());
        (self.output, self.dac)
    }
}

impl hal::dac::Dac<u16> for DAC {
    type Error = Infallible;
    fn write(&mut self, data: u16) -> Result<(), Self::Error> {
        self.dac
            .dhr12l1
            .write(|w| unsafe { w.bits(u32::from(data)) });
        Ok(())
    }
}
