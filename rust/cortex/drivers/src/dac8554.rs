//! This module implements the `DacArray` trait for the _DAC8554_ chip.

use core::result::Result;
use cortex_m::asm;
use hal::clock::Clock;
use hal::dac_array::DacArray;
use hal::pin::OutputPin;
use hal::spi::SpiTx;
use stm32f3xx_hal::hal::spi as espi;

#[derive(Debug)]
pub enum SendError<S, N> {
    Spi(S),
    Pin(N),
}

pub struct Dac8554<S, N> {
    spi: S,
    nss: N,
    addr: u8,
    sysclk: u32,
    transaction_open: bool,
}

pub const MODE: espi::Mode = espi::Mode {
    polarity: espi::Polarity::IdleLow,
    phase: espi::Phase::CaptureOnSecondTransition,
};

impl<S, N: OutputPin> Dac8554<S, N> {
    /// Create a new
    pub fn new<C: Clock>(spi: S, mut nss: N, clks: C, addr: u8) -> Result<Self, N::Error> {
        assert!(addr < 4);
        nss.set_high()?;
        Ok(Dac8554 {
            spi,
            nss,
            addr,
            sysclk: clks.sysclk_hz(),
            transaction_open: false,
        })
    }
}

impl<S: SpiTx<u8>, N: OutputPin> DacArray<u16, u8> for Dac8554<S, N> {
    type Error = SendError<S::Error, N::Error>;
    fn write(&mut self, val: u16, port: u8) -> Result<(), Self::Error> {
        assert!(port < 4);
        if self.transaction_open {
            self.spi.end_transaction();
            self.nss.set_high().map_err(SendError::Pin)?;
            asm::delay(self.sysclk / 25_000_000);
        }

        self.nss.set_low().map_err(SendError::Pin)?;
        self.spi.begin_transaction().map_err(SendError::Spi)?;
        self.spi
            .send((self.addr << 6) | 0x10 | (port << 1))
            .map_err(SendError::Spi)?;
        for byte in val.to_be_bytes().iter() {
            self.spi.send(*byte).map_err(SendError::Spi)?;
        }
        self.transaction_open = true;

        Ok(())
    }
}
