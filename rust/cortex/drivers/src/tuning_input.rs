use arrayvec::ArrayVec;
use pac::COMP;
use pac::TIM3;
use stm32f3xx_hal::gpio::gpioa::PA1;
use stm32f3xx_hal::gpio::Analog;
use stm32f3xx_hal::pac;
use stm32f3xx_hal::rcc::{APB1, APB2};

pub struct TuningInput {
    comp: COMP,
    tim: TIM3,
    pin: PA1<Analog>,
    clock: f32,
}

pub struct ActiveTuningInput {
    input: TuningInput,
    psc_locked: bool,
    samples: ArrayVec<u32, 32>,
}

fn underlying_enr1(_apb1: &mut APB1) -> &stm32f3xx_hal::pac::rcc::APB1ENR {
    unsafe { &(*stm32f3xx_hal::pac::RCC::ptr()).apb1enr }
}

fn underlying_enr2(_apb2: &mut APB2) -> &stm32f3xx_hal::pac::rcc::APB2ENR {
    unsafe { &(*stm32f3xx_hal::pac::RCC::ptr()).apb2enr }
}

impl TuningInput {
    pub fn new(
        apb1: &mut APB1,
        apb2: &mut APB2,
        comp: COMP,
        tim: TIM3,
        pin: PA1<Analog>,
        tim_clock: u32,
    ) -> TuningInput {
        underlying_enr1(apb1).modify(|_, w| w.tim3en().set_bit());
        underlying_enr2(apb2).modify(|_, w| w.syscfgen().set_bit());

        tim.ccmr1_input()
            .write(|w| w.cc1s().ti1().ic1f().fck_int_n8());
        tim.ccer.write(|w| w.cc1p().clear_bit().cc1np().clear_bit());
        tim.smcr.write(|w| w.ts().ti1fp1().sms().reset_mode());

        const TIMER3_INPUT_CAPTURE_1: u8 = 0b1010;
        const VREFINT: u8 = 0b011;
        const HIGH_HYST: u8 = 0b11;
        comp.comp1_csr.write(|w| unsafe {
            w.comp1outsel()
                .bits(TIMER3_INPUT_CAPTURE_1)
                .comp1inmsel()
                .bits(VREFINT)
                .comp1en()
                .set_bit()
                .comp1hyst()
                .bits(HIGH_HYST)
        });

        tim.arr.write(|w| w.arr().bits(0xFFFF));
        TuningInput {
            comp,
            tim,
            pin,
            clock: tim_clock as f32,
        }
    }

    pub fn activate(self) -> ActiveTuningInput {
        self.tim.psc.write(|w| w.psc().bits(1 << 10));
        self.tim.ccer.modify(|_, w| w.cc1e().set_bit());
        self.tim.cnt.write(|w| w.cnt().bits(0));
        self.tim.cr1.modify(|_, w| w.cen().set_bit());
        self.tim.dier.write(|w| w.cc1ie().set_bit());
        ActiveTuningInput {
            input: self,
            psc_locked: false,
            samples: ArrayVec::<_, 32>::new(),
        }
    }

    pub fn release(self) -> (COMP, TIM3, PA1<Analog>) {
        (self.comp, self.tim, self.pin)
    }
}

impl ActiveTuningInput {
    pub fn deactivate(self) -> TuningInput {
        self.input.tim.dier.write(|w| w);
        self.input.tim.ccer.modify(|_, w| w.cc1e().clear_bit());
        self.input.tim.cr1.modify(|_, w| w.cen().clear_bit());
        self.input
    }

    pub fn handle_interrupt(&mut self) {
        let measured = self.input.tim.ccr1.read().ccr().bits();

        if self.samples.is_full() {
            return;
        }

        if !self.psc_locked {
            self.psc_locked = true;
            return;
        }

        let psc = self.input.tim.psc.read().psc().bits();
        const DESIRED_RESOLUTION_MIN: u16 = 10000;
        if (measured < DESIRED_RESOLUTION_MIN) && (psc > 0) {
            self.psc_locked = false;
            self.input.tim.psc.write(|w| w.psc().bits(psc >> 1));
            return;
        }

        let scaled = (measured as u32) * (psc as u32 + 1);
        self.samples.push(scaled);
    }

    pub fn get_estimated_frequency(&mut self) -> Option<f32> {
        if !self.samples.is_full() {
            return Option::None;
        }

        self.samples.as_mut_slice().sort_unstable();
        const SKIP: usize = 8;
        let sum: f32 = self.samples.as_slice()[SKIP..(self.samples.len() - SKIP)]
            .iter()
            .map(|i| *i as f32)
            .sum();
        Option::Some(self.input.clock / sum * ((self.samples.len() - 2 * SKIP) as f32))
    }
}
