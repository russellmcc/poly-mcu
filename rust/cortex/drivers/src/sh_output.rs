use gpio::gpiob::PB8;
use gpio::AF1;
use hal::sample_hold::SampleHold;
use pac::TIM16;
use rcc::{Clocks, APB2};
use stm32f3xx_hal::{gpio, pac, rcc};

pub struct SHOutput<Otype> {
    tim: TIM16,
    output: PB8<AF1<Otype>>,
    top_freq: f32,
}

fn underlying_enr(_apb1: &mut APB2) -> &stm32f3xx_hal::pac::rcc::APB2ENR {
    unsafe { &(*stm32f3xx_hal::pac::RCC::ptr()).apb2enr }
}

// Note that we set this to 6 bits so that if the pll is 144MHz, the unscaled sample rate is 2MHz
// (This gives us okay frequency resolution for s/h sweeps using prescaler)
const NUM_PWM_BITS: u8 = 6;

impl<Otype> SHOutput<Otype> {
    pub fn new(
        tim: TIM16,
        apb2: &mut APB2,
        output: PB8<AF1<Otype>>,
        clocks: Clocks,
    ) -> SHOutput<Otype> {
        underlying_enr(apb2).modify(|_, w| w.tim16en().enabled());

        debug_assert!(unsafe { &(*stm32f3xx_hal::pac::RCC::ptr()) }
            .cr
            .read()
            .pllon()
            .is_on());

        unsafe { &(*stm32f3xx_hal::pac::RCC::ptr()) }
            .cfgr3
            .modify(|_, w| w.tim16sw().pll());

        tim.cr1.write(|w| w);
        tim.psc.write(|w| w);

        const PWM_MODE_1: u8 = 0b0110;
        tim.ccmr1_output()
            .write(|w| w.oc1m().bits(PWM_MODE_1).oc1m_3().clear_bit());
        tim.bdtr.modify(|_, w| w.moe().set_bit());
        tim.ccer.write(|w| w.cc1e().set_bit());
        tim.arr
            .write(|w| unsafe { w.arr().bits((1 << NUM_PWM_BITS) - 1) });

        tim.cr1.modify(|_, w| w.cen().enabled());
        let mut ret = SHOutput {
            tim,
            output,
            // The "2. *" here is because when pll is selected it's always multiplied by 2.
            top_freq: 2. * (clocks.sysclk().0 as f32) / ((1 << NUM_PWM_BITS) as f32),
        };
        ret.set_pitch(10.);
        ret.set_duty_cycle(1.);
        ret
    }

    pub fn free(self) -> (TIM16, PB8<AF1<Otype>>) {
        (self.tim, self.output)
    }
}

fn pitch_to_freq(pitch: f32) -> f32 {
    20. * libm::exp2f(pitch)
}

fn min(a: f32, b: f32) -> f32 {
    if a < b {
        a
    } else {
        b
    }
}

fn max(a: f32, b: f32) -> f32 {
    if a < b {
        b
    } else {
        a
    }
}

fn convert(f: f32) -> u16 {
    min(max(f, core::u16::MIN as f32), core::u16::MAX as f32) as u16
}

impl<Otype> SampleHold for SHOutput<Otype> {
    fn set_pitch(&mut self, pitch: f32) {
        let freq = pitch_to_freq(pitch);
        let mut divider = convert(self.top_freq / freq);
        divider = divider.saturating_sub(1);
        self.tim.psc.write(|w| w.psc().bits(divider));
    }

    fn set_duty_cycle(&mut self, duty: f32) {
        let duty_cycle = convert((1 << NUM_PWM_BITS) as f32 * duty);
        self.tim.ccr1.write(|w| unsafe { w.ccr().bits(duty_cycle) });
    }
}
