/// Implments `SpiTx` trait for any of the SPI peripherals on the stm32f3xx.
// Note this file is ported from stm32f30x-hal available here:
// https://github.com/japaric/stm32f30x-hal/blob/master/src/spi.rs
//
// Copyright (c) 2018 Jorge Aparicio
//
// Permission is hereby granted, free of charge, to any
// person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the
// Software without restriction, including without
// limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software
// is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice
// shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
// ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
// SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
use core::ptr;
use embedded_time::rate::Hertz;
use gpio::gpioa::{PA5, PA7};
use gpio::gpiob::{PB13, PB15, PB5};
use gpio::gpioc::{PC10, PC12};
use gpio::{AF5, AF6};
use hal::spi;
use pac::{RCC, SPI1, SPI2, SPI3};
use rcc::{Clocks, APB1, APB2};
use stm32f3xx_hal::hal::spi::{Mode, Phase, Polarity};
use stm32f3xx_hal::{gpio, pac, rcc};

/// SPI error
#[derive(Debug)]
#[non_exhaustive]
pub enum Error {
    /// Mode fault occurred
    ModeFault,
}

/// SCK pin -- DO NOT IMPLEMENT THIS TRAIT
pub trait SckPin<SPI>: private::Sealed {}

/// MOSI pin -- DO NOT IMPLEMENT THIS TRAIT
pub trait MosiPin<SPI>: private::Sealed {}

macro_rules! PinFuncs {
    ($($pin:ty: ($trait:ident, $spi:ty),)+) => {
        mod private {
            pub trait Sealed {}
            use super::*;
            $(impl<Otype> Sealed for $pin {})+
        }

        $(impl<Otype> $trait<$spi> for $pin {})+
    }
}

PinFuncs! {
    PA5<AF5<Otype>>: (SckPin, SPI1),
    PB13<AF5<Otype>>: (SckPin, SPI2),
    PC10<AF6<Otype>>: (SckPin, SPI3),
    PA7<AF5<Otype>>: (MosiPin, SPI1),
    PB5<AF5<Otype>>: (MosiPin, SPI1),
    PB15<AF5<Otype>>: (MosiPin, SPI2),
    PB5<AF6<Otype>>: (MosiPin, SPI3),
    PC12<AF6<Otype>>: (MosiPin, SPI3),
}

/// SPI peripheral operating in full duplex master mode
pub struct SpiTx<SPI, PINS> {
    spi: SPI,
    pins: PINS,
}

fn apb1_enr(_: &mut APB1) -> &pac::rcc::APB1ENR {
    // NOTE(unsafe) this proxy grants exclusive access to this register
    unsafe { &(*RCC::ptr()).apb1enr }
}

fn apb1_rstr(_: &mut APB1) -> &pac::rcc::APB1RSTR {
    // NOTE(unsafe) this proxy grants exclusive access to this register
    unsafe { &(*RCC::ptr()).apb1rstr }
}

fn apb2_enr(_: &mut APB2) -> &pac::rcc::APB2ENR {
    // NOTE(unsafe) this proxy grants exclusive access to this register
    unsafe { &(*RCC::ptr()).apb2enr }
}

fn apb2_rstr(_: &mut APB2) -> &pac::rcc::APB2RSTR {
    // NOTE(unsafe) this proxy grants exclusive access to this register
    unsafe { &(*RCC::ptr()).apb2rstr }
}

macro_rules! hal {
    ($($SPIX:ident: ($spiX:ident, $APBX:ident, $spiXen:ident, $spiXrst:ident, $pclkX:ident, $enr:ident, $rstr: ident),)+) => {
        $(
            impl<SCK, MOSI> SpiTx<$SPIX, (SCK, MOSI)> {
                /// Configures the SPI peripheral to operate in full duplex master mode
                pub fn $spiX<F>(
                    spi: $SPIX,
                    pins: (SCK, MOSI),
                    mode: Mode,
                    freq: F,
                    clocks: Clocks,
                    apb2: &mut $APBX,
                ) -> Self
                where
                    F: Into<Hertz>,
                    SCK: SckPin<$SPIX>,
                    MOSI: MosiPin<$SPIX>,
                {
                    // enable or reset $SPIX
                    $enr(apb2).modify(|_, w| w.$spiXen().enabled());
                    $rstr(apb2).modify(|_, w| w.$spiXrst().set_bit());
                    $rstr(apb2).modify(|_, w| w.$spiXrst().clear_bit());

                    // FRXTH: RXNE event is generated if the FIFO level is greater than or equal to
                    //        8-bit
                    // DS: 8-bit data size
                    // SSOE: Slave Select output disabled
                    spi.cr2
                        .write(|w| unsafe {
                            w.frxth().set_bit().ds().bits(0b111).ssoe().clear_bit()
                        });

                    let br = match clocks.$pclkX().0 / freq.into().0 {
                        0 => unreachable!(),
                        1..=2 => 0b000,
                        3..=5 => 0b001,
                        6..=11 => 0b010,
                        12..=23 => 0b011,
                        24..=39 => 0b100,
                        40..=95 => 0b101,
                        96..=191 => 0b110,
                        _ => 0b111,
                    };

                    // CPHA: phase
                    // CPOL: polarity
                    // MSTR: master mode
                    // BR: 1 MHz
                    // SPE: SPI disabled
                    // LSBFIRST: MSB first
                    // SSM: enable software slave management (NSS pin free for other uses)
                    // SSI: set nss high = master mode
                    // CRCEN: hardware CRC calculation disabled
                    // BIDIMODE: 2 line unidirectional (full duplex)
                    spi.cr1.write(|w| {
                        w.cpha()
                            .bit(mode.phase == Phase::CaptureOnSecondTransition)
                            .cpol()
                            .bit(mode.polarity == Polarity::IdleHigh)
                            .mstr()
                            .set_bit()
                            .br()
                            .bits(br)
                            .spe()
                            .set_bit()
                            .lsbfirst()
                            .clear_bit()
                            .ssi()
                            .set_bit()
                            .ssm()
                            .set_bit()
                            .crcen()
                            .clear_bit()
                            .bidimode()
                            .clear_bit()
                    });

                    SpiTx { spi, pins }
                }

                /// Releases the SPI peripheral and associated pins
                pub fn free(self) -> ($SPIX, (SCK, MOSI)) {
                    (self.spi, self.pins)
                }
            }

            impl<PINS> spi::SpiTx<u8> for SpiTx<$SPIX, PINS> {
                type Error = Error;

                fn begin_transaction(&mut self) -> Result<(), Self::Error> {
                    Ok(())
                }

                fn send(&mut self, byte: u8) -> Result<(), Self::Error> {
                    let sr = self.spi.sr.read();

                    Err(if sr.modf().bit_is_set() {
                        Error::ModeFault
                    } else {
                        while !self.spi.sr.read().txe().bit_is_set() {}
                        unsafe { ptr::write_volatile(&self.spi.dr as *const _ as *mut u8, byte) }
                        return Ok(());
                    })
                }

                fn end_transaction(&mut self) {
                    while self.spi.sr.read().ftlvl().bits() != 0 {}
                    while self.spi.sr.read().bsy().bit_is_set() {}
                }
            }
        )+
    }
}

hal! {
    SPI1: (spi1, APB2, spi1en, spi1rst, pclk2, apb2_enr, apb2_rstr),
    SPI2: (spi2, APB1, spi2en, spi2rst, pclk1, apb1_enr, apb1_rstr),
    SPI3: (spi3, APB1, spi3en, spi3rst, pclk1, apb1_enr, apb1_rstr),
}
