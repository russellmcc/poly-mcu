#![no_std]
#![no_main]

extern crate panic_halt;

#[rtic::app(device = stm32f3xx_hal::pac, peripherals = true, dispatchers = [USART3_EXTI28])]
mod app {
    use drivers::stm32f30x_spi_rx as spi_rx;
    use dwt_systick_monotonic::DwtSystick;
    use hal::spi::SpiSecondaryRx;
    use pac::USART2;
    use stm32f3xx_hal::gpio::gpiob::*;
    use stm32f3xx_hal::gpio::*;
    use stm32f3xx_hal::hal::spi as espi;
    use stm32f3xx_hal::serial::Tx;
    use stm32f3xx_hal::{nb, pac, prelude::*, serial};

    const MODE: espi::Mode = espi::Mode {
        polarity: espi::Polarity::IdleLow,
        phase: espi::Phase::CaptureOnFirstTransition,
    };

    #[shared]
    struct Shared {}

    #[local]
    struct Local {
        spi: spi_rx::SpiSecondaryRx<
            pac::SPI2,
            (
                PB13<AF5<PushPull>>,
                PB15<AF5<PushPull>>,
                PB12<AF5<PushPull>>,
            ),
        >,
        tx: Tx<USART2, PA2<AF7<PushPull>>>,
    }

    const SYSCLK_FREQ_MHZ: u32 = 72;
    const SYSCLK_FREQ_HZ: u32 = SYSCLK_FREQ_MHZ * 1_000_000;

    #[monotonic(binds = SysTick, default = true)]
    type MyMono = DwtSystick<{ SYSCLK_FREQ_HZ }>;

    #[init]
    fn init(mut cx: init::Context) -> (Shared, Local, init::Monotonics) {
        let device = cx.device;

        let mut rcc = device.RCC.constrain();
        let mut flash = device.FLASH.constrain();

        let clocks = rcc
            .cfgr
            .use_hse(8.MHz())
            .sysclk(SYSCLK_FREQ_MHZ.MHz())
            .pclk1(32.MHz())
            .pclk2(32.MHz())
            .freeze(&mut flash.acr);

        let mut gpioa = device.GPIOA.split(&mut rcc.ahb);
        let mut gpiob = device.GPIOB.split(&mut rcc.ahb);
        let sck =
            gpiob
                .pb13
                .into_af_push_pull(&mut gpiob.moder, &mut gpiob.otyper, &mut gpiob.afrh);
        let mosi =
            gpiob
                .pb15
                .into_af_push_pull(&mut gpiob.moder, &mut gpiob.otyper, &mut gpiob.afrh);
        let nss =
            gpiob
                .pb12
                .into_af_push_pull(&mut gpiob.moder, &mut gpiob.otyper, &mut gpiob.afrh);
        let spi = spi_rx::SpiSecondaryRx::spi2(device.SPI2, (sck, mosi, nss), MODE, &mut rcc.apb1);

        let (tx, _rx) = serial::Serial::new(
            device.USART2,
            (
                gpioa
                    .pa2
                    .into_af_push_pull(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrl),
                gpioa
                    .pa3
                    .into_af_push_pull(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrl),
            ),
            115200.Bd(),
            clocks,
            &mut rcc.apb1,
        )
        .split();
        (
            Shared {},
            Local { spi, tx },
            init::Monotonics(MyMono::new(
                &mut cx.core.DCB,
                cx.core.DWT,
                cx.core.SYST,
                SYSCLK_FREQ_HZ,
            )),
        )
    }

    #[idle(local = [tx, spi])]
    fn idle(cx: idle::Context) -> ! {
        loop {
            if let Some(word) = cx.local.spi.poll().unwrap() {
                nb::block!(cx.local.tx.write(word)).unwrap();
            }
        }
    }
}
