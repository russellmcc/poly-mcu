#![no_std]
#![no_main]

extern crate panic_halt;

#[rtic::app(device = stm32f3xx_hal::pac, peripherals = true, dispatchers = [USART3_EXTI28])]
mod app {
    use core::fmt::Write;
    use cortex_m::asm::delay;
    use drivers::tuning_input::*;
    use dwt_systick_monotonic::DwtSystick;
    use pac::USART2;
    use stm32f3xx_hal::gpio::*;
    use stm32f3xx_hal::rcc::Clocks;
    use stm32f3xx_hal::serial::{Rx, Tx};
    use stm32f3xx_hal::{hal, nb, pac, prelude::*, serial};

    #[shared]
    struct Shared {
        active: Option<ActiveTuningInput>,
    }

    #[local]
    struct Local {
        tuning_input: Option<TuningInput>,
        rx: Rx<USART2, PA3<AF7<PushPull>>>,
        tx: Tx<USART2, PA2<AF7<PushPull>>>,
        clocks: Clocks,
    }

    const SYSCLK_FREQ_MHZ: u32 = 72;
    const SYSCLK_FREQ_HZ: u32 = SYSCLK_FREQ_MHZ * 1_000_000;

    #[monotonic(binds = SysTick, default = true)]
    type MyMono = DwtSystick<{ SYSCLK_FREQ_HZ }>;

    #[init]
    fn init(mut cx: init::Context) -> (Shared, Local, init::Monotonics) {
        let device = cx.device;

        let mut rcc = device.RCC.constrain();
        let mut flash = device.FLASH.constrain();

        let clocks = rcc
            .cfgr
            .use_hse(8.MHz())
            .sysclk(SYSCLK_FREQ_MHZ.MHz())
            .pclk1(32.MHz())
            .pclk2(32.MHz())
            .freeze(&mut flash.acr);

        let mut gpioa = device.GPIOA.split(&mut rcc.ahb);
        let tuning_input = Option::Some(TuningInput::new(
            &mut rcc.apb1,
            &mut rcc.apb2,
            device.COMP,
            device.TIM3,
            gpioa.pa1.into_analog(&mut gpioa.moder, &mut gpioa.pupdr),
            clocks.pclk1().0 * 2,
        ));

        let (tx, rx) = serial::Serial::new(
            device.USART2,
            (
                gpioa
                    .pa2
                    .into_af_push_pull(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrl),
                gpioa
                    .pa3
                    .into_af_push_pull(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrl),
            ),
            115200.Bd(),
            clocks,
            &mut rcc.apb1,
        )
        .split();
        (
            Shared { active: None },
            Local {
                tx,
                rx,
                clocks,
                tuning_input,
            },
            init::Monotonics(MyMono::new(
                &mut cx.core.DCB,
                cx.core.DWT,
                cx.core.SYST,
                SYSCLK_FREQ_HZ,
            )),
        )
    }

    #[task(local = [tuning_input, tx, clocks], shared = [active])]
    fn measure(mut cx: measure::Context) {
        let active = &mut cx.shared.active;
        let tuning_input = cx.local.tuning_input;
        active.lock(|active| {
            *active = Option::Some(tuning_input.take().unwrap().activate());
        });

        let mut check = || active.lock(|active| active.as_mut().unwrap().get_estimated_frequency());

        let result = loop {
            if let Option::Some(result) = check() {
                break result;
            }
            delay(cx.local.clocks.sysclk().0 / 100);
        };
        active.lock(|active| {
            *tuning_input = Option::Some(active.take().unwrap().deactivate());
        });
        let tx: &mut dyn hal::serial::Write<_, Error = _> = cx.local.tx;
        write!(*tx, "frequency is: {}\r\n", result).unwrap();
    }

    #[task(binds = TIM3, priority = 2, shared = [active])]
    fn tim3(mut cx: tim3::Context) {
        cx.shared
            .active
            .lock(|active| active.as_mut().map(|x| x.handle_interrupt()));
    }

    #[idle(local = [rx])]
    fn idle(cx: idle::Context) -> ! {
        loop {
            match nb::block!(cx.local.rx.read()).unwrap() as char {
                'G' => measure::spawn().unwrap(),
                _ => {}
            }
        }
    }
}
