use hal::dac_array::DacArray;
use hal::pin::OutputPin;
use hal::sample_hold::SampleHold;

use tuning::TunedOutput;
use voice::{
    AnalogOutputPort, Hardware, SwitchedOutputPort, NUM_ANALOG_OUTPUT_PORTS,
    NUM_SWITCHED_OUTPUT_PORTS,
};

fn get_initial_tuning() -> [TunedOutput; NUM_ANALOG_OUTPUT_PORTS] {
    [
        // Osc1Pitch
        TunedOutput::default_tuning(0f32..=10f32),
        // Osc1Shape
        TunedOutput::default_tuning(0f32..=1.25f32),
        // Osc1Amp - inverted since connected to VCA
        TunedOutput::default_inverted_tuning(0f32..=1f32),
        // Osc2Pitch
        TunedOutput::default_tuning(0f32..=10f32),
        // Osc2Shape
        TunedOutput::default_tuning(0f32..=1.25f32),
        // Osc2Amp - inverted since connected to VCA
        TunedOutput::default_inverted_tuning(0f32..=1f32),
        // FilterRes - controls VCA connected to parameter where more
        //             current means less resonance, so NOT inverted.
        TunedOutput::default_tuning(0f32..=1f32),
        // Filter1Cutoff - inverted since connected to VCA
        TunedOutput::default_inverted_tuning(0f32..=10f32),
        // Filter2Cutoff - inverted since connected to VCA
        TunedOutput::default_inverted_tuning(0f32..=10f32),
        // Amp - inverted since connected to VCA
        TunedOutput::default_inverted_tuning(0f32..=1f32),
        // Noise
        TunedOutput::default_tuning(0f32..=1f32),
        // FM - inverted since connected to VCA
        TunedOutput::default_inverted_tuning(0f32..=1f32),
        // FilterFeedback - inverted since connected to VCA
        TunedOutput::default_inverted_tuning(0f32..=1f32),
        // Dummy elements - these outputs aren't connected to DACs but rather to the sample/hold
        // controller.
        // SampleHoldRate
        TunedOutput::default_tuning(0f32..=10f32),
        // SampleHoldDutyCycle
        TunedOutput::default_tuning(0f32..=1f32),
    ]
}

pub struct VoiceHardware<D, S, P> {
    dac_array: D,
    sample_hold: S,
    tuning_tables: [TunedOutput; NUM_ANALOG_OUTPUT_PORTS],

    switched_pins: [P; NUM_SWITCHED_OUTPUT_PORTS],
}

impl<D, S, P> VoiceHardware<D, S, P>
where
    D: DacArray<u16, u8>,
    S: SampleHold,
    P: OutputPin,
{
    pub fn new(
        d: D,
        s: S,
        switched_pins: [P; NUM_SWITCHED_OUTPUT_PORTS],
    ) -> VoiceHardware<D, S, P> {
        VoiceHardware {
            dac_array: d,
            sample_hold: s,
            tuning_tables: get_initial_tuning(),
            switched_pins,
        }
    }

    pub fn free(self) -> (D, S, [P; NUM_SWITCHED_OUTPUT_PORTS]) {
        (self.dac_array, self.sample_hold, self.switched_pins)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum VoiceHardwareError<D, P> {
    DacError(D),
    PinError(P),
}

impl<D, S, P> Hardware for VoiceHardware<D, S, P>
where
    D: DacArray<u16, u8>,
    S: SampleHold,
    P: OutputPin,
{
    type Error = VoiceHardwareError<D::Error, P::Error>;
    fn output_analog(&mut self, port: AnalogOutputPort, value: f32) -> Result<(), Self::Error> {
        match port {
            AnalogOutputPort::SampleHoldRate => {
                self.sample_hold.set_pitch(value);
                Ok(())
            }
            AnalogOutputPort::SampleHoldDutyCycle => {
                self.sample_hold.set_duty_cycle(value);
                Ok(())
            }
            _ => self
                .dac_array
                .write(self.tuning_tables[port as usize].output(value), port as u8)
                .map_err(VoiceHardwareError::DacError),
        }
    }

    fn output_switched(
        &mut self,
        port: SwitchedOutputPort,
        value: bool,
    ) -> Result<(), Self::Error> {
        let pin = &mut self.switched_pins[port as usize];
        (if value { pin.set_high() } else { pin.set_low() }).map_err(VoiceHardwareError::PinError)
    }
}
