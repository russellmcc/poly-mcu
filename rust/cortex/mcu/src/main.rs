#![no_std]
#![no_main]
#![deny(warnings)]
#![warn(nonstandard_style, future_incompatible, clippy::all)]

extern crate panic_halt;

mod hardware;
mod voice_hardware;

fn settings() -> voice::Settings {
    let mut ret: voice::Settings = Default::default();
    ret.osc_1.saw = true;
    ret.osc_1.volume = 1.;
    ret.filter_1.cutoff = 10.;
    ret.filter_2.cutoff = 10.;
    ret.sample_hold_duty_cycle = 1.;
    ret.amp_env.stages[0].target = 1.;
    ret.amp_env.stages[1].time = 0.5;
    ret.amp_volume = 1.0;
    ret
}

#[rtic::app(device = stm32f3xx_hal::pac, peripherals = true, dispatchers = [USART1_EXTI25])]
mod app {
    use crate::hardware;
    use crate::settings;
    use crate::voice_hardware;
    use dwt_systick_monotonic::DwtSystick;
    use embedded_time::rate::Extensions as TU32Ext;
    use rtic::rtic_monotonic::Monotonic;

    const DAC_RATE: f32 = 40_000.;
    const DAC_PERIOD_CYCLES: u32 = ((SYSCLK_FREQ_MHZ as f32) * 1_000_000. / DAC_RATE) as u32;

    const SYSCLK_FREQ_MHZ: u32 = 72;
    const SYSCLK_FREQ_HZ: u32 = SYSCLK_FREQ_MHZ * 1_000_000;

    // For testing we repeatedly trigger notes.
    const NOTE_RATE: f32 = 4.;
    const NOTE_PERIOD_CYCLES: u32 = ((SYSCLK_FREQ_MHZ as f32) * 1_000_000. / NOTE_RATE) as u32;

    #[shared]
    struct Shared {
        #[lock_free]
        voice: voice::Voice<
            voice_hardware::VoiceHardware<hardware::DacArray, hardware::SHOutput, hardware::Pin>,
        >,
    }

    #[local]
    struct Local {
        voice_hardware:
            voice_hardware::VoiceHardware<hardware::DacArray, hardware::SHOutput, hardware::Pin>,
        note_playing: bool,
        waited: bool,
    }

    #[monotonic(binds = SysTick, default = true)]
    type MyMono = DwtSystick<{ SYSCLK_FREQ_HZ }>;

    #[init]
    fn init(mut cx: init::Context) -> (Shared, Local, init::Monotonics) {
        let hardware = hardware::init_hardware(cx.device, SYSCLK_FREQ_MHZ.MHz());
        do_voice_tick::spawn().unwrap();
        virtual_note::spawn().unwrap();
        let mut voice_hardware = voice_hardware::VoiceHardware::new(
            hardware.dac_array,
            hardware.sh_output,
            hardware.switched_pins,
        );
        let voice = voice::Voice::new(DAC_RATE, &settings(), &mut voice_hardware).unwrap();
        (
            Shared { voice },
            Local {
                voice_hardware,
                note_playing: false,
                waited: false,
            },
            init::Monotonics(MyMono::new(
                &mut cx.core.DCB,
                cx.core.DWT,
                cx.core.SYST,
                SYSCLK_FREQ_HZ,
            )),
        )
    }

    #[task(shared = [voice], local = [voice_hardware])]
    fn do_voice_tick(cx: do_voice_tick::Context) {
        cx.shared.voice.tick(cx.local.voice_hardware).unwrap();
        do_voice_tick::spawn_after(<MyMono as Monotonic>::Duration::from_ticks(
            DAC_PERIOD_CYCLES,
        ))
        .unwrap();
    }

    #[task(shared = [voice], local = [note_playing, waited])]
    fn virtual_note(cx: virtual_note::Context) {
        virtual_note::spawn_after(<MyMono as Monotonic>::Duration::from_ticks(
            NOTE_PERIOD_CYCLES,
        ))
        .unwrap();
        if !(*cx.local.waited) {
            *cx.local.waited = true;
            return;
        }
        if *cx.local.note_playing {
            cx.shared.voice.note_off();
        } else {
            cx.shared.voice.note_on(3., 1.);
        }
        *cx.local.note_playing = !*cx.local.note_playing;
        *cx.local.waited = false;
    }
}
