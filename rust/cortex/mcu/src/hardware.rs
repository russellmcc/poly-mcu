use drivers::dac8554;
use drivers::pwm_outputs;
use drivers::pwm_outputs::PWMOutputs;
use drivers::sh_output;
use drivers::stm32f30x_dac::DAC;
use drivers::stm32f30x_spi;
use embedded_time::rate::Extensions as TU32Ext;
use hal::clock::Clock;
use hal::dac_array::*;
use hal::pin::OutputPin;
use stm32f3xx_hal::gpio::gpiob::*;
use stm32f3xx_hal::gpio::gpioc::*;
use stm32f3xx_hal::gpio::*;
use stm32f3xx_hal::pac;
use stm32f3xx_hal::prelude::*;
use voice::NUM_SWITCHED_OUTPUT_PORTS;

pub type DacArray = RemappedDacArray<
    CombinedDacArray<
        dac8554::Dac8554<
            stm32f30x_spi::SpiTx<pac::SPI2, (PB13<AF5<PushPull>>, PB15<AF5<PushPull>>)>,
            PB12<Output<PushPull>>,
        >,
        CombinedDacArray<
            SingleDacArray<DAC>,
            CombinedDacArray<
                PWMOutputs<
                    pac::TIM1,
                    PC0<AF2<PushPull>>,
                    PC1<AF2<PushPull>>,
                    PC2<AF2<PushPull>>,
                    PC3<AF2<PushPull>>,
                >,
                PWMOutputs<
                    pac::TIM8,
                    PC6<AF4<PushPull>>,
                    PC7<AF4<PushPull>>,
                    PC8<AF4<PushPull>>,
                    PC9<AF4<PushPull>>,
                >,
                u8,
            >,
            u8,
        >,
        u8,
    >,
    u8,
>;

pub type SHOutput = sh_output::SHOutput<PushPull>;
pub struct Pin(stm32f3xx_hal::gpio::PXx<Output<PushPull>>);

unsafe impl Send for Pin {}
impl OutputPin for Pin {
    type Error = <stm32f3xx_hal::gpio::PXx<Output<PushPull>> as OutputPin>::Error;

    fn set_low(&mut self) -> Result<(), Self::Error> {
        self.0.set_low()
    }

    fn set_high(&mut self) -> Result<(), Self::Error> {
        self.0.set_high()
    }
}

pub struct Hardware {
    pub dac_array: DacArray,
    pub sh_output: SHOutput,
    pub switched_pins: [Pin; NUM_SWITCHED_OUTPUT_PORTS],
}

#[derive(Copy, Clone)]
struct STM32F3Clock {
    clocks: stm32f3xx_hal::rcc::Clocks,
}

impl Clock for STM32F3Clock {
    fn sysclk_hz(&self) -> u32 {
        self.clocks.sysclk().0
    }
}

const OUTPUT_MAP: [u8; 13] = [
    0,  // Osc1Pitch,
    5,  // Osc1Shape,
    6,  // Osc1Amp,
    1,  // Osc2Pitch,
    7,  // Osc2Shape,
    8,  // Osc2Amp,
    9,  // FilterRes,
    2,  // Filter1Cutoff,
    3,  // Filter2Cutoff,
    10, // Amp,
    4,  // Noise,
    11, // FM,
    12, // FilterFeedback
];

pub fn init_hardware<T: Into<embedded_time::rate::Megahertz>>(
    device: pac::Peripherals,
    sysclk: T,
) -> Hardware {
    let mut flash = device.FLASH.constrain();
    let mut rcc = device.RCC.constrain();

    let sysclk_hz = sysclk.into();
    let clocks = rcc
        .cfgr
        .sysclk(sysclk_hz)
        .pclk1(32.MHz())
        .freeze(&mut flash.acr);

    let mut gpioa = device.GPIOA.split(&mut rcc.ahb);
    let mut gpiob = device.GPIOB.split(&mut rcc.ahb);
    let mut gpioc = device.GPIOC.split(&mut rcc.ahb);

    // Note that the SPI device of the stm does support NSS on its own, however we split out the
    // NSS so we can write a generic driver in the future.
    let nss = gpiob
        .pb12
        .into_push_pull_output(&mut gpiob.moder, &mut gpiob.otyper);
    let sck = gpiob
        .pb13
        .into_af_push_pull(&mut gpiob.moder, &mut gpiob.otyper, &mut gpiob.afrh);
    let mosi = gpiob
        .pb15
        .into_af_push_pull(&mut gpiob.moder, &mut gpiob.otyper, &mut gpiob.afrh);

    let spi = stm32f30x_spi::SpiTx::spi2(
        device.SPI2,
        (sck, mosi),
        dac8554::MODE,
        10_000_000.Hz(),
        clocks,
        &mut rcc.apb1,
    );

    // PA1 is reserved for the tuning input - this isn't hooked up yet, but we need to make it analog.
    gpioa.pa1.into_analog(&mut gpioa.moder, &mut gpioa.pupdr);

    macro_rules! make_output {
        ($pax:ident, $gpio:ident) => {
            Pin($gpio
                .$pax
                .into_push_pull_output(&mut $gpio.moder, &mut $gpio.otyper)
                .downgrade()
                .downgrade())
        };
    }

    Hardware {
        dac_array: RemappedDacArray::new(
            CombinedDacArray::new(
                dac8554::Dac8554::new(spi, nss, STM32F3Clock { clocks }, 0).unwrap(),
                CombinedDacArray::new(
                    SingleDacArray::new(DAC::new(
                        gpioa.pa4.into_analog(&mut gpioa.moder, &mut gpioa.pupdr),
                        device.DAC1,
                        &mut rcc.apb1,
                    )),
                    CombinedDacArray::new(
                        PWMOutputs::<pac::TIM1, _, _, _, _>::new(
                            device.TIM1,
                            &mut rcc.apb2,
                            pwm_outputs::Outputs {
                                o1: gpioc.pc0.into_af_push_pull(
                                    &mut gpioc.moder,
                                    &mut gpioc.otyper,
                                    &mut gpioc.afrl,
                                ),
                                o2: gpioc.pc1.into_af_push_pull(
                                    &mut gpioc.moder,
                                    &mut gpioc.otyper,
                                    &mut gpioc.afrl,
                                ),
                                o3: gpioc.pc2.into_af_push_pull(
                                    &mut gpioc.moder,
                                    &mut gpioc.otyper,
                                    &mut gpioc.afrl,
                                ),
                                o4: gpioc.pc3.into_af_push_pull(
                                    &mut gpioc.moder,
                                    &mut gpioc.otyper,
                                    &mut gpioc.afrl,
                                ),
                            },
                            clocks,
                        ),
                        PWMOutputs::<pac::TIM8, _, _, _, _>::new(
                            device.TIM8,
                            &mut rcc.apb2,
                            pwm_outputs::Outputs {
                                o1: gpioc.pc6.into_af_push_pull(
                                    &mut gpioc.moder,
                                    &mut gpioc.otyper,
                                    &mut gpioc.afrl,
                                ),
                                o2: gpioc.pc7.into_af_push_pull(
                                    &mut gpioc.moder,
                                    &mut gpioc.otyper,
                                    &mut gpioc.afrl,
                                ),
                                o3: gpioc.pc8.into_af_push_pull(
                                    &mut gpioc.moder,
                                    &mut gpioc.otyper,
                                    &mut gpioc.afrh,
                                ),
                                o4: gpioc.pc9.into_af_push_pull(
                                    &mut gpioc.moder,
                                    &mut gpioc.otyper,
                                    &mut gpioc.afrh,
                                ),
                            },
                            clocks,
                        ),
                        4,
                    ),
                    1,
                ),
                4,
            ),
            &OUTPUT_MAP,
        ),
        sh_output: SHOutput::new(
            device.TIM16,
            &mut rcc.apb2,
            gpiob
                .pb8
                .into_af_push_pull(&mut gpiob.moder, &mut gpiob.otyper, &mut gpiob.afrh),
            clocks,
        ),
        switched_pins: [
            make_output!(pa0, gpioa),
            make_output!(pb4, gpiob),
            make_output!(pa2, gpioa),
            make_output!(pa3, gpioa),
            make_output!(pa5, gpioa),
            make_output!(pa6, gpioa),
            make_output!(pa7, gpioa),
            make_output!(pa8, gpioa),
            make_output!(pb6, gpiob),
            make_output!(pb7, gpiob),
            make_output!(pa11, gpioa),
            make_output!(pa12, gpioa),
            make_output!(pb5, gpiob),
            make_output!(pb0, gpiob),
            make_output!(pb1, gpiob),
            make_output!(pb2, gpiob),
        ],
    }
}
