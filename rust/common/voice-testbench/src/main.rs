#![warn(nonstandard_style, rust_2018_idioms, future_incompatible)]
#![deny(warnings)]

use plotters::prelude::*;
use std::ops;
use voice::{AnalogOutputPort, Hardware, SwitchedOutputPort, Voice, NUM_ANALOG_OUTPUT_PORTS};

struct OutputLane {
    range: ops::Range<f32>,
    name: &'static str,
}

const LANE_INFOS: [OutputLane; NUM_ANALOG_OUTPUT_PORTS] = [
    OutputLane {
        range: 0f32..10f32,
        name: "osc 1 freq",
    },
    OutputLane {
        range: 0f32..1f32,
        name: "osc 1 shape",
    },
    OutputLane {
        range: 0f32..1f32,
        name: "osc 1 amp",
    },
    OutputLane {
        range: 0f32..10f32,
        name: "osc 2 freq",
    },
    OutputLane {
        range: 0f32..1f32,
        name: "osc 2 shape",
    },
    OutputLane {
        range: 0f32..1f32,
        name: "osc 2 amp",
    },
    OutputLane {
        range: 0f32..1f32,
        name: "filter res",
    },
    OutputLane {
        range: 0f32..10f32,
        name: "filter 1 cutoff",
    },
    OutputLane {
        range: 0f32..10f32,
        name: "filter 2 cutoff",
    },
    OutputLane {
        range: 0f32..1f32,
        name: "amp",
    },
    OutputLane {
        range: -1f32..1f32,
        name: "noise",
    },
    OutputLane {
        range: 0f32..1f32,
        name: "fm",
    },
    OutputLane {
        range: 0f32..1f32,
        name: "filter feedback",
    },
    OutputLane {
        range: 0f32..10f32,
        name: "sample hold pitch",
    },
    OutputLane {
        range: 0f32..1f32,
        name: "sample hold duty cycle",
    },
];

#[derive(Debug, Default)]
struct Grapher {
    curr_time: f32,
    outputs: [Vec<(f32, f32)>; NUM_ANALOG_OUTPUT_PORTS],
}

impl Hardware for Grapher {
    type Error = ();
    fn output_analog(&mut self, port: AnalogOutputPort, value: f32) -> Result<(), ()> {
        self.outputs[port as usize].push((self.curr_time, value));
        Ok(())
    }
    fn output_switched(&mut self, _port: SwitchedOutputPort, _value: bool) -> Result<(), ()> {
        // Currently we don't display switched outputs
        Ok(())
    }
}

fn settings() -> voice::Settings {
    let mut ret: voice::Settings = Default::default();
    ret.mod_env.stages[0].target = 1.;
    ret.mod_env.stages[1].time = 0.5;
    ret.osc_1.pitch_srcs[1] = (voice::ModSource::ModEnv, 2.);
    ret.osc_1.vibrato = 1.;
    ret.noise_level = 0.2;
    ret.vibrato_lfo_rate = 2.;
    ret
}

const TICK_RATE: f32 = 2000f32;
const GATE_TIME: f32 = 3.;
const RELEASE_TIME: f32 = 1.5;
const TOTAL_TIME: f32 = GATE_TIME + RELEASE_TIME;

fn tick_graphed_voice(voice: &mut Voice<Grapher>, grapher: &mut Grapher, tick_rate: f32) {
    grapher.curr_time += 1. / tick_rate;
    voice.tick(grapher).unwrap();
}

fn tick_graphed_voice_for_time(
    voice: &mut Voice<Grapher>,
    grapher: &mut Grapher,
    tick_rate: f32,
    time: f32,
) {
    for _ in 0..((time * tick_rate).round() as usize) {
        tick_graphed_voice(voice, grapher, tick_rate);
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let root =
        BitMapBackend::new("0.png", (640, 240 * (LANE_INFOS.len() as u32))).into_drawing_area();

    root.fill(&WHITE)?;

    let lanes = root.split_evenly((LANE_INFOS.len(), 1));
    let mut grapher = Grapher::default();
    let mut voice = Voice::new(TICK_RATE, &settings(), &mut grapher).unwrap();
    voice.note_on(3.5, 0.5);
    tick_graphed_voice_for_time(&mut voice, &mut grapher, TICK_RATE, GATE_TIME);
    voice.note_off();
    tick_graphed_voice_for_time(&mut voice, &mut grapher, TICK_RATE, RELEASE_TIME);
    let time_range = 0f32..TOTAL_TIME;
    for (lane_index, (info, lane)) in LANE_INFOS.iter().zip(lanes.iter()).enumerate() {
        let mut chart = ChartBuilder::on(lane)
            .x_label_area_size(30)
            .y_label_area_size(50)
            .caption(info.name, ("Arial", 24).into_font())
            .build_cartesian_2d(time_range.clone(), info.range.clone())?;

        chart.configure_mesh().draw()?;

        chart.draw_series(LineSeries::new(
            grapher.outputs[lane_index].iter().cloned(),
            RED,
        ))?;
    }
    Ok(())
}
