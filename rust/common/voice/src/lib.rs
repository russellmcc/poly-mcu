#![cfg_attr(not(test), no_std)]
#![warn(nonstandard_style, rust_2018_idioms, future_incompatible, clippy::all)]
#![deny(warnings)]
#![allow(clippy::type_complexity)]

mod envelope;
mod gate_listener;
mod hal;
mod lcg32;
mod lfo;
pub mod settings;
use crate::gate_listener::GateListener;
pub use hal::{
    AnalogOutputPort, Hardware, SwitchedOutputPort, NUM_ANALOG_OUTPUT_PORTS,
    NUM_SWITCHED_OUTPUT_PORTS,
};
use lcg32::Lcg32;
use rand::distributions::Distribution;
use rand::SeedableRng;
pub use settings::{ModSource, Settings, USER_SRCS};
extern crate static_assertions as sa;

const NUM_MOD_SRCS: usize = 11;
const MOD_FREQ_SCALAR: f32 = 1. / (NUM_MOD_SRCS as f32);
const NUM_MOD_DESTS: usize = 15;

const ALL_SRCS: usize = USER_SRCS + 2;

struct VoiceMods {
    amp_env: envelope::Envelope,
    filter_env: envelope::Envelope,
    mod_env: envelope::Envelope,
    pulse_width_lfo: lfo::Lfo,
    vibrato_lfo: lfo::Lfo,
    lfo_1: lfo::Lfo,
    lfo_2: lfo::Lfo,
    note_pitch: f32,
    velocity: f32,
    after_touch: f32,
    mod_wheel: f32,
}

#[derive(Clone)]
struct ModSrcData {
    tick: fn(&mut VoiceMods) -> f32,
    value: f32,
}

#[derive(Debug, Clone, PartialEq)]
struct ModDestSettings {
    base_val: f32,
    mod_srcs: [(ModSource, f32); ALL_SRCS],
}

#[derive(Clone)]
struct ModDest<H: Hardware> {
    output: fn(f32, &mut Voice<H>, &mut H) -> Result<(), H::Error>,
    bounds: core::ops::RangeInclusive<f32>,
    settings: ModDestSettings,
}

fn trim(x: f32, range: &core::ops::RangeInclusive<f32>) -> f32 {
    if x < *range.start() {
        *range.start()
    } else if x > *range.end() {
        *range.end()
    } else {
        x
    }
}

/// `Voice` represents all the state of a current synth voice.
///
/// We assume that we output DAC outputs at a fixed rate.
///
/// Note that the individual signals (`Amp`, `Osc1Freq`, etc.), might be output at different schedules.
///
/// It acts as an `Iterator`, outputing a `VoiceOutput` per tick.
pub struct Voice<H: Hardware> {
    mods: VoiceMods,

    mod_srcs: [ModSrcData; NUM_MOD_SRCS],
    mod_dests: [ModDest<H>; NUM_MOD_DESTS],
    next_mod_dest: usize,

    rng: Lcg32,
    noise_dist: rand::distributions::Uniform<f32>,

    active_settings: Settings,
}

fn basic_lfo(rate: f32) -> settings::lfo::Settings {
    settings::lfo::Settings {
        shape: settings::lfo::Shape::Triangle,
        rate,
        phase_mode: settings::lfo::PhaseMode::FreeRunning,
        delay: 0.,
        ramp_time: 0.,
    }
}

fn mod_dests_for_settings<H: hal::Hardware>(settings: &Settings) -> [ModDest<H>; NUM_MOD_DESTS] {
    macro_rules! mod_dest {
            ($x:ident, $b:expr, $($base:ident).+, $($mods:ident).+) => {
                ModDest {
                    output: |f, _, h|  h.output_analog(AnalogOutputPort::$x, f),
                    bounds: $b,
                    settings: ModDestSettings {
                        base_val: settings.$($base).+,
                        mod_srcs: [settings.$($mods).+[0],
                                   settings.$($mods).+[1],
                                   settings.$($mods).+[2],
                                   (ModSource::default(), 0f32),
                                   (ModSource::default(), 0f32),]
                    }
                }
            };
            ($x:ident, $b:expr, $($base:ident).+, $($mods:ident).+, $($builtin:ident).+, $($builtin_amount:ident).+) => {
                ModDest {
                    output: |f, _, h| h.output_analog(AnalogOutputPort::$x, f),
                    bounds: $b,
                    settings: ModDestSettings {
                        base_val: settings.$($base).+,
                        mod_srcs: [settings.$($mods).+[0],
                                   settings.$($mods).+[1],
                                   settings.$($mods).+[2],
                                   (ModSource::default(), 0f32),
                                   (ModSource::$($builtin).+, settings.$($builtin_amount).+)]
                    }
                }
            };
            ($x:ident, $b:expr, $($base:ident).+, $($mods:ident).+, $($builtin:ident).+, $builtin_amount:expr) => {
                ModDest {
                    output: |f, _, h|  h.output_analog(AnalogOutputPort::$x, f),
                    bounds: $b,
                    settings: ModDestSettings {
                        base_val: settings.$($base).+,
                        mod_srcs: [settings.$($mods).+[0],
                                   settings.$($mods).+[1],
                                   settings.$($mods).+[2],
                                   (ModSource::default(), 0f32),
                                   (ModSource::$($builtin).+, $builtin_amount)]
                    }
                }
            };
        ($x:ident,
         $b:expr,
         $($base:ident).+,
         $($mods:ident).+,
         $($builtin_1:ident).+,
         $($builtin_amount_1:ident).+,
         $($builtin_2:ident).+,
         $($builtin_amount_2:ident).+) => {
                ModDest {
                    output: |f, _, h|  h.output_analog(AnalogOutputPort::$x, f),
                    bounds: $b,
                    settings: ModDestSettings {
                        base_val: settings.$($base).+,
                        mod_srcs: [settings.$($mods).+[0],
                                   settings.$($mods).+[1],
                                   settings.$($mods).+[2],
                                   (ModSource::$($builtin_1).+, settings.$($builtin_amount_1).+),
                                   (ModSource::$($builtin_2).+, settings.$($builtin_amount_2).+)]
                    }
                }
            };
        ($x:ident,
         $b:expr,
         $($base:ident).+,
         $($mods:ident).+,
         $($builtin_1:ident).+,
         $($builtin_amount_1:ident).+,
         $($builtin_2:ident).+,
         $builtin_amount_2:expr) => {
                ModDest {
                    output: |f, _, h|  h.output_analog(AnalogOutputPort::$x, f),
                    bounds: $b,
                    settings: ModDestSettings {
                        base_val: settings.$($base).+,
                        mod_srcs: [settings.$($mods).+[0],
                                   settings.$($mods).+[1],
                                   settings.$($mods).+[2],
                                   (ModSource::$($builtin_1).+, settings.$($builtin_amount_1).+),
                                   (ModSource::$($builtin_2).+, $builtin_amount_2)]
                    }
                }
            };
            ($x:ident, $b:expr, $base:expr, $($mods:ident).+, $($builtin:ident).+, $($builtin_amount:ident).+) => {
                ModDest {
                    output: |f, _, h|  h.output_analog(AnalogOutputPort::$x, f),
                    bounds: $b,
                    settings: ModDestSettings {
                        base_val: $base,
                        mod_srcs: [settings.$($mods).+[0],
                                   settings.$($mods).+[1],
                                   settings.$($mods).+[2],
                                   (ModSource::default(), 0f32),
                                   (ModSource::$($builtin).+, settings.$($builtin_amount).+)]
                    }
                }
            };
        }
    [
        mod_dest!(
            Osc1Pitch,
            0f32..=10f32,
            osc_1.pitch,
            osc_1.pitch_srcs,
            VibratoLFO,
            osc_1.vibrato,
            NotePitch,
            1.
        ),
        mod_dest!(
            Osc1Shape,
            0f32..=1f32,
            osc_1.pulse_width,
            osc_1.pulse_width_mod_srcs,
            PulseWidthLFO,
            osc_1.pulse_width_mod_amount
        ),
        mod_dest!(Osc1Amp, 0f32..=1f32, osc_1.volume, osc_1.volume_srcs),
        mod_dest!(
            Osc2Pitch,
            0f32..=10f32,
            osc_2.pitch,
            osc_2.pitch_srcs,
            VibratoLFO,
            osc_2.vibrato,
            NotePitch,
            1.
        ),
        mod_dest!(
            Osc2Shape,
            0f32..=1f32,
            osc_2.pulse_width,
            osc_2.pulse_width_mod_srcs,
            PulseWidthLFO,
            osc_2.pulse_width_mod_amount
        ),
        mod_dest!(Osc2Amp, 0f32..=1f32, osc_2.volume, osc_2.volume_srcs),
        mod_dest!(FM, 0f32..=1f32, osc_fm, osc_fm_srcs),
        ModDest {
            output: |f, v, h| {
                h.output_analog(AnalogOutputPort::Noise, f * v.noise_dist.sample(&mut v.rng))
            },
            bounds: 0f32..=1f32,
            settings: ModDestSettings {
                base_val: settings.noise_level,
                mod_srcs: [
                    settings.noise_level_srcs[0],
                    settings.noise_level_srcs[1],
                    settings.noise_level_srcs[2],
                    (ModSource::default(), 0f32),
                    (ModSource::default(), 0f32),
                ],
            },
        },
        mod_dest!(FilterRes, 0f32..=1f32, filter_res, filter_res_srcs),
        mod_dest!(
            Filter1Cutoff,
            0f32..=10f32,
            filter_1.cutoff,
            filter_1.cutoff_srcs,
            FilterEnv,
            filter_1.env_amount,
            NotePitch,
            filter_key_follow
        ),
        mod_dest!(
            Filter2Cutoff,
            0f32..=10f32,
            filter_2.cutoff,
            filter_2.cutoff_srcs,
            FilterEnv,
            filter_2.env_amount,
            NotePitch,
            filter_key_follow
        ),
        mod_dest!(Amp, 0f32..=1f32, 0f32, amp_srcs, AmpEnv, amp_volume),
        mod_dest!(
            FilterFeedback,
            0f32..=1f32,
            filter_feedback,
            filter_feedback_srcs
        ),
        mod_dest!(
            SampleHoldRate,
            0f32..=10f32,
            sample_hold_rate,
            sample_hold_rate_srcs
        ),
        mod_dest!(
            SampleHoldDutyCycle,
            0f32..=1f32,
            sample_hold_duty_cycle,
            sample_hold_rate_srcs
        ),
    ]
}

macro_rules! forward_listeners {
    ($v:expr, $action:ident) => {
        $v.amp_env.$action();
        $v.filter_env.$action();
        $v.mod_env.$action();
        $v.pulse_width_lfo.$action();
        $v.vibrato_lfo.$action();
        $v.lfo_1.$action();
        $v.lfo_2.$action();
    };
}

fn output_if_changed<H: Hardware>(
    new_settings: &Settings,
    old_settings: Option<&Settings>,
    hardware: &mut H,
) -> Result<(), H::Error> {
    fn get_filter_mode_a(t: settings::FilterType) -> bool {
        ((t as u8) & 0b1) == 0u8
    }

    fn get_filter_mode_b(t: settings::FilterType) -> bool {
        ((t as u8) & 0b10) == 0u8
    }

    fn maybe_output<H: Hardware>(
        hardware: &mut H,
        port: SwitchedOutputPort,
        val: bool,
        maybe_old_val: Option<bool>,
    ) -> Result<(), H::Error> {
        if let Some(old_val) = maybe_old_val {
            if old_val == val {
                return Ok(());
            }
        }
        hardware.output_switched(port, val)
    }

    use SwitchedOutputPort::*;
    macro_rules! handle_port {
        ($port:expr, $get_val:expr) => {
            maybe_output(
                hardware,
                $port,
                $get_val(new_settings),
                old_settings.map($get_val),
            )?;
        };
    }

    handle_port!(Osc1Pulse, |settings: &Settings| settings.osc_1.pulse);
    handle_port!(Osc1Saw, |settings: &Settings| settings.osc_1.saw);
    handle_port!(Osc1Tri, |settings: &Settings| settings.osc_1.tri);
    handle_port!(Osc1Waveshaping, |settings: &Settings| settings
        .osc_1
        .wave_shaping);

    handle_port!(Osc2Pulse, |settings: &Settings| settings.osc_2.pulse);
    handle_port!(Osc2Saw, |settings: &Settings| settings.osc_2.saw);
    handle_port!(Osc2Tri, |settings: &Settings| settings.osc_2.tri);
    handle_port!(Osc2Waveshaping, |settings: &Settings| settings
        .osc_2
        .wave_shaping);

    handle_port!(HardSync, |settings: &Settings| settings.hard_sync);

    handle_port!(Filter1ModeA, |settings: &Settings| get_filter_mode_a(
        settings.filter_1.filter_type
    ));
    handle_port!(Filter1ModeB, |settings: &Settings| get_filter_mode_b(
        settings.filter_1.filter_type
    ));
    handle_port!(Filter1Disable, |settings: &Settings| settings
        .filter_1
        .filter_type
        == settings::FilterType::Disabled);

    handle_port!(Filter2ModeA, |settings: &Settings| get_filter_mode_a(
        settings.filter_2.filter_type
    ));
    handle_port!(Filter2ModeB, |settings: &Settings| get_filter_mode_b(
        settings.filter_2.filter_type
    ));
    handle_port!(Filter2Disable, |settings: &Settings| settings
        .filter_2
        .filter_type
        == settings::FilterType::Disabled);
    handle_port!(
        FilterIsParallel,
        |settings: &Settings| match settings.filter_routing {
            settings::FilterRouting::Parallel => true,
            settings::FilterRouting::Series => false,
        }
    );
    Ok(())
}

fn init_switched_settings<H: Hardware>(
    settings: &Settings,
    hardware: &mut H,
) -> Result<(), H::Error> {
    output_if_changed(settings, None, hardware)
}

impl<H: Hardware> Voice<H> {
    /// Constructs a new `Voice` with the given tick rate.  We expect `ticks` to be called at the given rate.
    pub fn new(
        tick_rate: f32,
        settings: &Settings,
        hardware: &mut H,
    ) -> Result<Voice<H>, H::Error> {
        let src_rate = tick_rate * MOD_FREQ_SCALAR;

        macro_rules! tickable_mod_src {
            ($x:ident) => {
                ModSrcData {
                    tick: |v: &mut VoiceMods| v.$x.tick(),
                    value: 0f32,
                }
            };
        }

        macro_rules! const_mod_src {
            ($x:ident) => {
                ModSrcData {
                    tick: |v: &mut VoiceMods| v.$x,
                    value: 0.,
                }
            };
        }

        init_switched_settings(settings, hardware)?;
        Ok(Voice {
            mods: VoiceMods {
                amp_env: envelope::Envelope::new(src_rate, &settings.amp_env),
                filter_env: envelope::Envelope::new(src_rate, &settings.filter_env),
                mod_env: envelope::Envelope::new(src_rate, &settings.mod_env),

                pulse_width_lfo: lfo::Lfo::new(src_rate, &basic_lfo(settings.pulse_width_lfo_rate)),
                vibrato_lfo: lfo::Lfo::new(src_rate, &basic_lfo(settings.vibrato_lfo_rate)),
                lfo_1: lfo::Lfo::new(src_rate, &settings.lfo_1),
                lfo_2: lfo::Lfo::new(src_rate, &settings.lfo_2),
                note_pitch: 0.,
                velocity: 0.,
                after_touch: 0.,
                mod_wheel: 0.,
            },

            mod_srcs: [
                tickable_mod_src!(amp_env),
                tickable_mod_src!(filter_env),
                tickable_mod_src!(mod_env),
                tickable_mod_src!(pulse_width_lfo),
                tickable_mod_src!(vibrato_lfo),
                tickable_mod_src!(lfo_1),
                tickable_mod_src!(lfo_2),
                const_mod_src!(note_pitch),
                const_mod_src!(velocity),
                const_mod_src!(after_touch),
                const_mod_src!(mod_wheel),
            ],
            mod_dests: mod_dests_for_settings(settings),
            next_mod_dest: 0,
            rng: Lcg32::seed_from_u64(0),
            noise_dist: rand::distributions::Uniform::from(-1f32..=1f32),
            active_settings: settings.clone(),
        })
    }

    pub fn tick(&mut self, hardware: &mut H) -> Result<(), H::Error> {
        sa::const_assert!(NUM_MOD_DESTS > NUM_MOD_SRCS);

        if self.next_mod_dest < NUM_MOD_SRCS {
            let mod_src = &mut self.mod_srcs[self.next_mod_dest];
            mod_src.value = (mod_src.tick)(&mut self.mods);
        }
        let output_val = {
            let mod_dest = &mut self.mod_dests[self.next_mod_dest];
            let mut output_val = mod_dest.settings.base_val;

            for (src, amount) in &mod_dest.settings.mod_srcs {
                output_val += self.mod_srcs[*src as usize].value * amount;
            }
            output_val
        };
        let mod_dest = &self.mod_dests[self.next_mod_dest];
        self.next_mod_dest = (self.next_mod_dest + 1) % self.mod_dests.len();
        (mod_dest.output)(trim(output_val, &mod_dest.bounds), self, hardware)
    }

    #[allow(clippy::float_cmp)]
    pub fn update_settings(
        &mut self,
        new_settings: &Settings,
        hardware: &mut H,
    ) -> Result<(), H::Error> {
        if new_settings.amp_env != self.active_settings.amp_env {
            self.mods.amp_env.update_settings(&new_settings.amp_env);
        }

        if new_settings.filter_env != self.active_settings.filter_env {
            self.mods
                .filter_env
                .update_settings(&new_settings.filter_env);
        }

        if new_settings.mod_env != self.active_settings.mod_env {
            self.mods.mod_env.update_settings(&new_settings.mod_env);
        }

        if new_settings.vibrato_lfo_rate != self.active_settings.vibrato_lfo_rate {
            self.mods
                .vibrato_lfo
                .update_settings(&basic_lfo(new_settings.vibrato_lfo_rate))
        }

        if new_settings.pulse_width_lfo_rate != self.active_settings.pulse_width_lfo_rate {
            self.mods
                .pulse_width_lfo
                .update_settings(&basic_lfo(new_settings.pulse_width_lfo_rate))
        }

        if new_settings.lfo_1 != self.active_settings.lfo_1 {
            self.mods.lfo_1.update_settings(&new_settings.lfo_1);
        }

        if new_settings.lfo_2 != self.active_settings.lfo_2 {
            self.mods.lfo_2.update_settings(&new_settings.lfo_2);
        }

        self.mod_dests = mod_dests_for_settings(new_settings);

        output_if_changed(new_settings, Some(&self.active_settings), hardware)?;
        self.active_settings = new_settings.clone();
        Ok(())
    }

    pub fn note_on(&mut self, note_pitch: f32, velocity: f32) {
        self.mods.note_pitch = note_pitch;
        self.mods.velocity = velocity;
        forward_listeners!(self.mods, note_on);
    }

    pub fn note_off(&mut self) {
        forward_listeners!(self.mods, note_off);
    }

    pub fn set_mod_wheel(&mut self, mod_wheel: f32) {
        self.mods.mod_wheel = mod_wheel;
    }

    pub fn set_after_touch(&mut self, after_touch: f32) {
        self.mods.after_touch = after_touch;
    }
}

fn abs(x: f32) -> f32 {
    if x >= 0f32 {
        x
    } else {
        -x
    }
}

fn signum(x: f32) -> f32 {
    if x.is_sign_positive() {
        1f32
    } else {
        -1f32
    }
}

fn min(a: f32, b: f32) -> f32 {
    if a < b {
        a
    } else {
        b
    }
}

fn max(a: f32, b: f32) -> f32 {
    if a < b {
        b
    } else {
        a
    }
}

#[cfg(test)]
pub mod tests;
