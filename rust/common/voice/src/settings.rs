pub use crate::envelope::settings as envelope;
pub use crate::lfo::settings as lfo;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ModSource {
    AmpEnv,
    FilterEnv,
    ModEnv,
    PulseWidthLFO,
    VibratoLFO,
    LFO1,
    LFO2,
    NotePitch,
    Velocity,
    AfterTouch,
    ModWheel,
}

impl Default for ModSource {
    fn default() -> ModSource {
        ModSource::LFO1
    }
}

pub const USER_SRCS: usize = 3;

#[derive(Debug, Clone, Default, PartialEq)]
pub struct OscillatorSettings {
    pub pitch: f32,
    pub vibrato: f32,
    pub pitch_srcs: [(ModSource, f32); USER_SRCS],
    pub pulse: bool,
    pub saw: bool,
    pub tri: bool,
    pub pulse_width: f32,
    pub pulse_width_mod_amount: f32,
    pub pulse_width_mod_srcs: [(ModSource, f32); USER_SRCS],
    pub wave_shaping: bool,
    pub volume: f32,
    pub volume_srcs: [(ModSource, f32); USER_SRCS],
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum FilterType {
    LowPass,
    BandPass,
    HighPass,
    Disabled,
}

impl Default for FilterType {
    fn default() -> FilterType {
        FilterType::LowPass
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum FilterRouting {
    Series,
    Parallel,
}

impl Default for FilterRouting {
    fn default() -> FilterRouting {
        FilterRouting::Series
    }
}

#[derive(Debug, Clone, Default, PartialEq)]
pub struct FilterSettings {
    pub cutoff: f32,
    pub filter_type: FilterType,
    pub env_amount: f32,
    pub cutoff_srcs: [(ModSource, f32); USER_SRCS],
}

#[derive(Debug, Clone, Default)]
pub struct Settings {
    pub amp_env: envelope::Settings,
    pub filter_env: envelope::Settings,
    pub mod_env: envelope::Settings,

    pub pulse_width_lfo_rate: f32,
    pub vibrato_lfo_rate: f32,

    pub lfo_1: lfo::Settings,
    pub lfo_2: lfo::Settings,

    pub osc_1: OscillatorSettings,
    pub osc_2: OscillatorSettings,
    pub hard_sync: bool,

    pub osc_fm: f32,
    pub osc_fm_srcs: [(ModSource, f32); USER_SRCS],

    pub noise_level: f32,
    pub noise_level_srcs: [(ModSource, f32); USER_SRCS],

    pub filter_res: f32,
    pub filter_res_srcs: [(ModSource, f32); USER_SRCS],

    pub filter_feedback: f32,
    pub filter_feedback_srcs: [(ModSource, f32); USER_SRCS],

    pub filter_key_follow: f32,
    pub filter_1: FilterSettings,
    pub filter_2: FilterSettings,
    pub filter_routing: FilterRouting,

    pub amp_volume: f32,
    pub amp_srcs: [(ModSource, f32); USER_SRCS],

    pub sample_hold_rate: f32,
    pub sample_hold_rate_srcs: [(ModSource, f32); USER_SRCS],

    pub sample_hold_duty_cycle: f32,
    pub sample_hold_duty_cycle_srcs: [(ModSource, f32); USER_SRCS],
}
