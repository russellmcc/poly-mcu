use rand::{RngCore, SeedableRng};
use rand_core::impls::*;

/// Super-fast, really low-quality LCG-based RNG.
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Lcg32 {
    state: u32,
}

impl RngCore for Lcg32 {
    fn next_u32(&mut self) -> u32 {
        self.state = self
            .state
            .wrapping_mul(1_664_525)
            .wrapping_add(1_013_904_223);
        self.state
    }

    fn next_u64(&mut self) -> u64 {
        next_u64_via_u32(self)
    }

    fn fill_bytes(&mut self, dest: &mut [u8]) {
        fill_bytes_via_next(self, dest);
    }

    fn try_fill_bytes(&mut self, dest: &mut [u8]) -> Result<(), rand_core::Error> {
        self.fill_bytes(dest);
        Ok(())
    }
}

impl SeedableRng for Lcg32 {
    type Seed = [u8; 4];

    fn from_seed(seed: [u8; 4]) -> Lcg32 {
        Lcg32 {
            state: u32::from_le_bytes(seed),
        }
    }
}
