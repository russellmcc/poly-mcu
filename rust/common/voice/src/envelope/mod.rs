use super::abs;
use crate::gate_listener::GateListener;
use core::f32;

pub mod settings;
use settings::*;

mod stage;
use stage::Stage;
use stage::Tickable;

fn loop_point_to_stage(l: LoopPoint) -> usize {
    match l {
        LoopPoint::Stage1 => 0,
        LoopPoint::Stage2 => 1,
    }
}

#[derive(Debug, Default, Clone)]
struct StageCoeffs {
    stage_idx: Option<usize>,

    stage: Stage,

    target: f32,
    start_point: f32,
    /// Only meaningful during "back and forth" loop modes, says which direction we going.
    stage_incr: isize,
    boundary: usize,
}

/// An alpha-juno style envelope with a constant rate.
#[derive(Debug, Clone)]
pub struct Envelope {
    rate: f32,
    level: f32,
    stages: [StageSettings; STAGES],
    coeffs: StageCoeffs,
    release_time: f32,
    release_curvature: f32,
    loop_mode: LoopMode,
}

/// The level at which we consider the target of an envelope reached.
const EPSILON: f32 = 1e-4;

impl Envelope {
    pub fn new(rate: f32, settings: &Settings) -> Envelope {
        let mut ret = Envelope {
            rate,
            level: 0f32,
            stages: Default::default(),
            coeffs: Default::default(),
            release_time: 0f32,
            release_curvature: 0f32,
            loop_mode: LoopMode::None,
        };
        ret.update_settings(settings);
        ret
    }

    pub fn update_settings(&mut self, settings: &Settings) {
        self.stages = settings.stages.clone();
        for stage in self.stages.iter_mut() {
            stage.time *= self.rate;
        }
        self.release_time = settings.release_time * self.rate;
        self.release_curvature = settings.release_curvature;
        self.loop_mode = settings.loop_mode;

        // Correct current target/step_size.
        match self.coeffs.stage_idx {
            Some(idx) => {
                self.coeffs.target = self.stages[idx].target;
                if self.stages[idx].time == 0f32 {
                    self.level = self.stages[idx].target;
                }
                self.coeffs.stage = Stage::new(
                    self.coeffs.start_point,
                    self.coeffs.target,
                    self.stages[idx].time,
                    self.stages[idx].curvature,
                );
            }
            None => {
                self.coeffs.target = 0.;
                if self.release_time == 0f32 {
                    self.level = 0.;
                }
                self.coeffs.stage = Stage::new(
                    self.coeffs.start_point,
                    self.coeffs.target,
                    self.release_time,
                    self.release_curvature,
                );
            }
        }

        // Correct looping logic - stop looping backwards for back and forth mode.
        match self.loop_mode {
            LoopMode::BackAndForth => {}
            _ => {
                self.coeffs.stage_incr = 1;
                self.coeffs.boundary = STAGES - 1;
            }
        }
    }

    pub fn tick(&mut self) -> f32 {
        fn maybe_advance_stage(env: &mut Envelope) -> Option<()> {
            fn update_coeffs_for_stage(
                level: &mut f32,
                stages: &[StageSettings; 3],
                coeffs: &mut StageCoeffs,
                stage_idx: usize,
            ) {
                coeffs.stage_idx = Some(stage_idx);
                let stage = &stages[stage_idx];
                coeffs.target = stage.target;
                if stage.time == 0f32 {
                    *level = stage.target;
                }
                coeffs.start_point = *level;
                coeffs.stage = Stage::new(
                    coeffs.start_point,
                    coeffs.target,
                    stage.time,
                    stage.curvature,
                );
            }
            let mut stage_idx = env.coeffs.stage_idx?;
            while abs(env.coeffs.target - env.level) < EPSILON {
                let inside_limit = if env.coeffs.stage_incr > 0 {
                    stage_idx < env.coeffs.boundary
                } else {
                    stage_idx > env.coeffs.boundary
                };

                if inside_limit {
                    stage_idx = ((stage_idx as isize) + env.coeffs.stage_incr) as usize;
                    update_coeffs_for_stage(
                        &mut env.level,
                        &env.stages,
                        &mut env.coeffs,
                        stage_idx,
                    );
                } else {
                    // loop city!
                    match env.loop_mode {
                        LoopMode::None => {
                            env.coeffs.stage = Stage::null();
                            break;
                        }
                        LoopMode::BackAndForth => {
                            env.coeffs.stage_incr = -env.coeffs.stage_incr;
                            env.coeffs.boundary = if env.coeffs.stage_incr > 0 {
                                STAGES - 1
                            } else {
                                0
                            };
                        }
                        LoopMode::Loop(l) => {
                            stage_idx = loop_point_to_stage(l);
                            update_coeffs_for_stage(
                                &mut env.level,
                                &env.stages,
                                &mut env.coeffs,
                                stage_idx,
                            );
                        }
                    }
                }
            }
            Some(())
        }

        self.level = if self.level < self.coeffs.target {
            crate::min(self.coeffs.target, self.coeffs.stage.tick(self.level))
        } else {
            crate::max(self.coeffs.target, self.coeffs.stage.tick(self.level))
        };
        maybe_advance_stage(self);
        self.level
    }
}

impl Iterator for &mut Envelope {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.tick())
    }
}

impl GateListener for Envelope {
    fn note_on(&mut self) {
        if self.stages[0].time < EPSILON {
            self.level = self.stages[0].target;
        }
        self.coeffs = StageCoeffs {
            stage_idx: Some(0),
            stage_incr: 1,
            target: self.stages[0].target,
            boundary: STAGES - 1,
            start_point: 0.,
            stage: Stage::new(
                0.,
                self.stages[0].target,
                self.stages[0].time,
                self.stages[0].curvature,
            ),
        }
    }

    fn note_off(&mut self) {
        if self.release_time < EPSILON {
            self.level = 0.;
        }

        self.coeffs = StageCoeffs {
            stage_idx: None,
            stage_incr: 1,
            target: 0f32,
            boundary: STAGES - 1,
            start_point: self.level,
            stage: Stage::new(self.level, 0., self.release_time, self.release_curvature),
        }
    }
}

#[cfg(test)]
pub mod tests;
