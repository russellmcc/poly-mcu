#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum LoopPoint {
    /// Loop between stage 1 and stage 3
    Stage1,

    /// Loop between stage 2 and stage 3
    Stage2,
}

#[derive(Debug, Clone, Default, PartialEq)]
pub struct StageSettings {
    /// Time (in seconds) this stage lasts.
    pub time: f32,

    /// The level (0->1) this stage achieves when it is finished.
    pub target: f32,

    /// Arbitrary "curvature" (-1 -> 1) of this stage.
    ///
    /// At 0, this is a linear stage.
    /// Below 0, at 50% of the time of the stage we'll be at a value less than halfway
    /// between the starting point of the stage and the next stage.
    /// Above 0, at 50% of the time of the stage we'll be at a value more than the halfway point.
    pub curvature: f32,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum LoopMode {
    None,
    Loop(LoopPoint),
    BackAndForth,
}

impl Default for LoopMode {
    fn default() -> LoopMode {
        LoopMode::None
    }
}

pub const STAGES: usize = 3;

#[derive(Default, Debug, Clone, PartialEq)]
pub struct Settings {
    pub stages: [StageSettings; STAGES],
    pub release_time: f32,
    pub release_curvature: f32,
    pub loop_mode: LoopMode,
}
