use super::*;
use proptest::prelude::*;

pub fn stage(
    time: impl Strategy<Value = f32>,
    target: impl Strategy<Value = f32>,
    curvature: impl Strategy<Value = f32>,
) -> impl Strategy<Value = StageSettings> {
    (time, target, curvature).prop_map(|(time, target, curvature)| StageSettings {
        time,
        target,
        curvature,
    })
}

pub fn arb_stage() -> impl Strategy<Value = StageSettings> {
    stage(0f32..1000f32, 0f32..1f32, -1f32..1f32)
}

pub fn arb_loop_point() -> impl Strategy<Value = LoopPoint> {
    prop_oneof![Just(LoopPoint::Stage1), Just(LoopPoint::Stage2)]
}

pub fn arb_loop_mode() -> impl Strategy<Value = LoopMode> {
    prop_oneof![
        Just(LoopMode::None),
        arb_loop_point().prop_map(LoopMode::Loop),
        Just(LoopMode::BackAndForth)
    ]
}

pub fn arb_settings() -> impl Strategy<Value = Settings> {
    (
        arb_stage(),
        arb_stage(),
        arb_stage(),
        0f32..1000f32,
        -1f32..1f32,
        arb_loop_mode(),
    )
        .prop_map(
            |(a, b, c, release_time, release_curvature, loop_mode)| Settings {
                stages: [a, b, c],
                release_time,
                release_curvature,
                loop_mode,
            },
        )
}

proptest! {
    #[test]
    fn starts_silent(a in arb_settings()) {
        let mut env_a = Envelope::new(1., &a);
        assert_eq!(env_a.tick(), 0f32)
    }

    #[test]
    fn linear_attack_moves_faster_with_shorter_time(mut a in arb_settings(), mut b in arb_settings()) {
        for x in &mut [&mut a, &mut b] {
        x.stages[0].target = 1.;
            x.stages[0].curvature = 0.;
        }

        let mut env_a = Envelope::new(1., &a);
        env_a.note_on();
        let a_first = env_a.tick();
        let mut env_b = Envelope::new(1., &b);
        env_b.note_on();
        let b_first = env_b.tick();
        assert_eq!(a_first > b_first, b.stages[0].time > a.stages[0].time);
    }

    #[test]
    fn attack_time_accurate(mut a in arb_settings()) {
        a.stages[0].target = 1.;
        a.stages[0].curvature = 0.;
        a.stages[0].time = (a.stages[0].time as usize + 1) as f32;

        let mut env_a = Envelope::new(1., &a);
        env_a.note_on();
        for _ in 0..(a.stages[0].time as usize - 1) {
            env_a.tick();
        }
        let level = env_a.tick();
        assert!(abs(level - 1f32) < EPSILON, "{:?}", level);
    }

    #[test]
    fn sustain_level_accurate(mut a in arb_settings()) {
        a.loop_mode = LoopMode::None;
        let mut env_a = Envelope::new(1., &a);
        env_a.note_on();
        for _ in 0..((a.stages[0].time + a.stages[1].time + a.stages[2].time) as usize + 100) {
            env_a.tick();
        }
        let level = env_a.tick();
        assert!(abs(level - a.stages[2].target) < EPSILON, "{:?} {:?} {:?}", level, a.stages[2].target, env_a);
    }

    #[test]
    fn release_time_accurate(mut a in arb_settings()) {
        a.stages[2].target = 1.;
        a.loop_mode = LoopMode::None;
        // add a few ticks of slop for approximate math in curvature calculations
        let slop = 5f32;
        a.release_time += slop;
        a.release_curvature = 0.0;
        let mut env_a = Envelope::new(1., &a);
        env_a.note_on();
        for _ in 0..5000 {
            env_a.tick();
        }
        env_a.note_off();

        for _ in 0..((a.release_time - slop) as usize) {
            env_a.tick();
        }
        assert!(env_a.tick() > EPSILON);
        for _ in 0..((a.release_time + 2.*slop) as usize) {
            env_a.tick();
        }
        assert!(env_a.tick() < EPSILON);
    }
}
