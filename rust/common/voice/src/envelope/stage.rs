use super::abs;
use super::EPSILON;

pub trait Tickable {
    fn tick(&self, state: f32) -> f32;
}

#[derive(Debug, Default, Clone)]
struct LinearStage {
    step: f32,
}

impl LinearStage {
    fn new(start: f32, end: f32, time: f32) -> LinearStage {
        LinearStage {
            step: (end - start) / time,
        }
    }
}

impl Tickable for LinearStage {
    fn tick(&self, state: f32) -> f32 {
        state + self.step
    }
}

/// Note the exponential stage works on a principle where we have an internal variable
/// $s$ which = $e^{kt}$ for $t\elem[0, time]$. This internal variable is related to the
/// envelope's level $l$ with the relation $l = a + bs$.
///
/// Note that to avoid discontinuities during live parameter changes, we only store the
/// level $l$ between samples, so each samples we invert the above formula to get $s$ for
/// the last $l$.
#[derive(Debug, Default, Clone)]
struct ExponentialStage {
    b: f32,
    inv_b: f32,
    k: f32,
    a: f32,
}

impl ExponentialStage {
    fn new(start: f32, end: f32, time: f32, curvature: f32) -> ExponentialStage {
        let k = -5. * curvature / time;
        let b = (end - start) / (libm::expf(k * time) - 1.);
        ExponentialStage {
            b,
            k,
            inv_b: 1. / b,
            a: start - b,
        }
    }
}

impl Tickable for ExponentialStage {
    fn tick(&self, state: f32) -> f32 {
        // convert $l$ to $s$ via above formula
        let mut s = (state - self.a) * self.inv_b;

        // $s= exp^{kt} => $\frac{ds}{dt} = ks$
        s += s * self.k;

        // output $l$.
        self.b * s + self.a
    }
}

#[derive(Debug, Default, Clone)]
struct NullStage {}

impl Tickable for NullStage {
    fn tick(&self, state: f32) -> f32 {
        state
    }
}

#[derive(Debug, Clone)]
enum AnyStage {
    Null(NullStage),
    Linear(LinearStage),
    Exponential(ExponentialStage),
}

impl Default for AnyStage {
    fn default() -> AnyStage {
        AnyStage::Null(Default::default())
    }
}

#[derive(Default, Debug, Clone)]
pub struct Stage(AnyStage);

impl Tickable for Stage {
    fn tick(&self, state: f32) -> f32 {
        match &self.0 {
            AnyStage::Exponential(e) => e.tick(state),
            AnyStage::Linear(l) => l.tick(state),
            AnyStage::Null(n) => n.tick(state),
        }
    }
}

impl Stage {
    pub fn null() -> Stage {
        Stage(AnyStage::Null(NullStage {}))
    }

    pub fn new(start: f32, end: f32, time: f32, curvature: f32) -> Stage {
        if abs(start - end) < EPSILON {
            return Stage(AnyStage::Null(NullStage {}));
        }

        if time < EPSILON {
            return Stage(AnyStage::Null(NullStage {}));
        }

        let has_curvature = abs(curvature) >= 10. * EPSILON;

        // if the start and end are too close, it doesn't make sense to use a curved stage and may give us numerical trouble
        let not_too_tiny = abs(start - end) >= 10. * EPSILON;

        if has_curvature && not_too_tiny {
            Stage(AnyStage::Exponential(ExponentialStage::new(
                start, end, time, curvature,
            )))
        } else {
            Stage(AnyStage::Linear(LinearStage::new(start, end, time)))
        }
    }
}
