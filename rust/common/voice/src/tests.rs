use crate::hal::*;
use core::convert::Infallible;

#[derive(Default)]
struct SwitchedTestHardware {
    values: [bool; crate::NUM_SWITCHED_OUTPUT_PORTS],
    assert_on_repeated_set: bool,
}

impl SwitchedTestHardware {
    fn is_set(&self, port: SwitchedOutputPort) -> bool {
        self.values[port as usize]
    }
}

impl crate::hal::Hardware for SwitchedTestHardware {
    type Error = Infallible;
    fn output_analog(&mut self, _port: AnalogOutputPort, _value: f32) -> Result<(), Self::Error> {
        Ok(())
    }
    fn output_switched(
        &mut self,
        port: SwitchedOutputPort,
        value: bool,
    ) -> Result<(), Self::Error> {
        if self.assert_on_repeated_set {
            assert!(value != self.is_set(port));
        }
        self.values[port as usize] = value;
        Ok(())
    }
}

#[test]
fn init_state_hard_sync_off() {
    let mut test_hardware = SwitchedTestHardware::default();
    test_hardware
        .output_switched(SwitchedOutputPort::HardSync, true)
        .unwrap();
    let settings = crate::settings::Settings::default();
    crate::Voice::new(1f32, &settings, &mut test_hardware).unwrap();
    assert!(!test_hardware.is_set(SwitchedOutputPort::HardSync));
}

#[test]
fn can_turn_hard_sync_on() {
    let mut test_hardware = SwitchedTestHardware::default();
    let mut settings = crate::settings::Settings::default();
    let mut voice = crate::Voice::new(1f32, &settings, &mut test_hardware).unwrap();
    settings.hard_sync = true;
    test_hardware.assert_on_repeated_set = true;
    voice
        .update_settings(&settings, &mut test_hardware)
        .unwrap();
    assert!(test_hardware.is_set(SwitchedOutputPort::HardSync));
}
