//! This module contains abstraction traits for the hardware.

pub const NUM_ANALOG_OUTPUT_PORTS: usize = 15;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
/// Represents an analog output signal the voice might want to send to the hardware.
pub enum AnalogOutputPort {
    Osc1Pitch,
    Osc1Shape,
    Osc1Amp,
    Osc2Pitch,
    Osc2Shape,
    Osc2Amp,
    FilterRes,
    Filter1Cutoff,
    Filter2Cutoff,
    Amp,
    Noise,
    FM,
    FilterFeedback,
    SampleHoldRate,
    SampleHoldDutyCycle,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
/// Represents a switched (on or off) signal that the voice might want to send to the hardware.
pub enum SwitchedOutputPort {
    Osc1Pulse,
    Osc1Saw,
    Osc1Tri,
    Osc2Pulse,
    Osc2Saw,
    Osc2Tri,
    HardSync,
    Osc1Waveshaping,
    Osc2Waveshaping,
    Filter1ModeA,
    Filter1ModeB,
    Filter2ModeA,
    Filter2ModeB,
    FilterIsParallel,
    Filter1Disable,
    Filter2Disable,
}

pub const NUM_SWITCHED_OUTPUT_PORTS: usize = 16;

pub trait Hardware {
    type Error;

    fn output_analog(&mut self, port: AnalogOutputPort, value: f32) -> Result<(), Self::Error>;
    fn output_switched(&mut self, port: SwitchedOutputPort, value: bool)
        -> Result<(), Self::Error>;
}
