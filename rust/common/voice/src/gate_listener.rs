pub trait GateListener {
    fn note_on(&mut self);
    fn note_off(&mut self);
}
