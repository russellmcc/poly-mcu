pub mod settings;

use settings::*;

use super::abs;
use super::signum;
use crate::gate_listener::GateListener;

#[derive(Debug, Clone)]
enum State {
    Delaying(f32),
    Running(f32),
}

#[derive(Debug, Clone)]
struct ResetInfo {
    level: f32,
    incr_signum: f32,
}

/// Basic, bipolar LFO with delay and shape functionality.
#[derive(Debug, Clone)]
pub struct Lfo {
    rate: f32,
    shape: Shape,
    incr: f32,
    level: f32,
    delay: f32,
    reset_point: Option<ResetInfo>,
    ramp_incr: Option<f32>,
    state: State,
}

impl Lfo {
    pub fn new(rate: f32, settings: &Settings) -> Lfo {
        let mut lfo = Lfo {
            rate,
            shape: Default::default(),
            incr: 0.,
            level: 0.,
            delay: 0.,
            reset_point: None,
            ramp_incr: None,
            state: State::Running(1.),
        };

        lfo.update_settings(settings);
        lfo
    }

    pub fn update_settings(&mut self, settings: &Settings) {
        self.shape = settings.shape;
        self.incr = match self.shape {
            Shape::Triangle => signum(self.incr) * settings.rate / self.rate * 4.,
            Shape::RampUp | Shape::Square => settings.rate / self.rate * 2.,
            Shape::RampDown => -settings.rate / self.rate * 2.,
        };
        if abs(self.incr) > 2f32 {
            self.incr = signum(self.incr) * 2f32;
        }
        self.delay = settings.delay * self.rate;
        self.reset_point = match settings.phase_mode {
            PhaseMode::FreeRunning => None,
            PhaseMode::Reset(x) => Some(match self.shape {
                Shape::Triangle => {
                    if x < 0.5 {
                        ResetInfo {
                            level: (x * 4.) - 1.,
                            incr_signum: 1.,
                        }
                    } else {
                        ResetInfo {
                            level: 1. - (x - 0.5) * 4.,
                            incr_signum: -1.,
                        }
                    }
                }
                Shape::RampUp | Shape::Square => ResetInfo {
                    level: x * 2. - 1.,
                    incr_signum: 1.,
                },
                Shape::RampDown => ResetInfo {
                    level: 1. - x * 2.,
                    incr_signum: -1.,
                },
            }),
        };
        self.ramp_incr = if settings.ramp_time > 0f32 {
            Some(1. / (settings.ramp_time * self.rate))
        } else {
            None
        };
    }

    fn handle_finish_delay(&mut self) {
        if self.ramp_incr.is_some() {
            self.state = State::Running(0.);
        } else {
            self.state = State::Running(1.);
        }
        if let Some(ref reset) = self.reset_point {
            self.level = reset.level;
            self.incr = reset.incr_signum * abs(self.incr);
        }
    }

    fn clamp_to_range(&mut self) {
        while !(-1f32..=1f32).contains(&self.level) {
            if self.level > 1. {
                match self.shape {
                    Shape::Triangle => {
                        self.incr = -abs(self.incr);
                        self.level = 2f32 - self.level;
                    }
                    Shape::RampDown => {
                        // err...
                        self.level = 1.;
                    }
                    Shape::RampUp | Shape::Square => {
                        self.level -= 2.;
                    }
                }
            } else {
                match self.shape {
                    Shape::Triangle => {
                        self.incr = abs(self.incr);
                        self.level = -2f32 - self.level;
                    }
                    Shape::RampDown => {
                        self.level += 2.;
                    }
                    Shape::RampUp | Shape::Square => {
                        // err...
                        self.level = -1.;
                    }
                }
            }
        }
    }

    pub fn tick(&mut self) -> f32 {
        let run = |lfo: &mut Lfo, mut ramp_level: f32| {
            ramp_level = match lfo.ramp_incr {
                Some(ramp_incr) => crate::min(ramp_level + ramp_incr, 1f32),
                None => 1.,
            };
            lfo.state = State::Running(ramp_level);
            lfo.level += lfo.incr;

            // Ensure level is in range.
            lfo.clamp_to_range();

            ramp_level
                * (if lfo.shape == Shape::Square {
                    if lfo.level > 0. {
                        1.
                    } else {
                        -1.
                    }
                } else {
                    lfo.level
                })
        };
        match self.state {
            State::Delaying(t) => {
                if t < self.delay {
                    self.state = State::Delaying(t + 1.);
                    return 0f32;
                };

                self.handle_finish_delay();
                if let State::Running(ramp_level) = self.state {
                    run(self, ramp_level)
                } else {
                    panic!("didn't start running...")
                }
            }
            State::Running(ramp_level) => run(self, ramp_level),
        }
    }
}

impl Iterator for &mut Lfo {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.tick())
    }
}

impl GateListener for Lfo {
    fn note_on(&mut self) {
        if self.delay > 0f32 {
            self.state = State::Delaying(0.);
        } else {
            self.handle_finish_delay();
        }
    }

    fn note_off(&mut self) {}
}
