#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Shape {
    Triangle,
    RampUp,
    RampDown,
    Square,
}

impl Default for Shape {
    fn default() -> Shape {
        Shape::Triangle
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum PhaseMode {
    /// LFO will continue to run and will not reset
    FreeRunning,

    /// The LFO will reset at every note-on to the specified phase
    Reset(f32),
}

impl Default for PhaseMode {
    fn default() -> PhaseMode {
        PhaseMode::FreeRunning
    }
}

#[derive(Default, Debug, Clone, PartialEq)]
pub struct Settings {
    pub shape: Shape,
    pub rate: f32,
    pub phase_mode: PhaseMode,

    /// If non-0, on each gate the LFO will be silent for this period.  phase reset,
    /// if active, will occur after the delay
    pub delay: f32,

    /// If non-0, the LFO will "ramp-in" over this period in seconds
    pub ramp_time: f32,
}
