pub trait Clock {
    fn sysclk_hz(&self) -> u32;
}
