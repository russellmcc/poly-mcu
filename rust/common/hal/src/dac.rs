pub trait Dac<Word> {
    type Error;
    fn write(&mut self, data: Word) -> Result<(), Self::Error>;
}
