pub trait SampleHold {
    /// Sets the "pitch" of the sample hold.  This is octave-scaled, such that
    /// 0 = 20hz, 1 = 40hz, 2 = 80hz, etc.
    fn set_pitch(&mut self, pitch: f32);

    /// Sets the duty cycle from 0->1 of the sample hold.  1 means the signal will be passed through.
    fn set_duty_cycle(&mut self, duty_cycle: f32);
}
