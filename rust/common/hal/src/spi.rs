/// TX-only SPI Primary
pub trait SpiTx<Word> {
    type Error;
    fn begin_transaction(&mut self) -> Result<(), Self::Error>;

    /// Only valid to call during a transaction
    fn send(&mut self, data: Word) -> Result<(), Self::Error>;

    fn end_transaction(&mut self);
}

/// RX-only SPI Secondary
pub trait SpiSecondaryRx<Word> {
    type Error;

    fn poll(&mut self) -> Result<Option<Word>, Self::Error>;
}
