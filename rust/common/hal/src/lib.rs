#![cfg_attr(not(test), no_std)]
#![deny(warnings)]
#![warn(nonstandard_style, rust_2018_idioms, future_incompatible)]

//! Modules containing hardware abstraction traits for various peripherals.
pub mod clock;
pub mod dac;
pub mod dac_array;
pub mod pin;
pub mod sample_hold;
pub mod spi;
