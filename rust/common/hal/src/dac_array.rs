use crate::dac::Dac;
use core::convert::Infallible;

pub trait DacArray<Word, Port> {
    type Error;
    fn write(&mut self, val: Word, port: Port) -> Result<(), Self::Error>;
}

/// This helper creates a [DacArray] that doesn't output anything
/// ```
/// # use hal::dac_array::*;
/// let mut nullArray = NullDacArray{};
/// // doesn't do anything, always succeeds
/// assert_eq!(Ok(()), nullArray.write(3u8, 3u8));
/// ```
pub struct NullDacArray {}

impl<Word, Port> DacArray<Word, Port> for NullDacArray {
    type Error = Infallible;
    fn write(&mut self, _: Word, _: Port) -> Result<(), Self::Error> {
        Ok(())
    }
}

#[derive(Debug, PartialEq)]
pub enum CombinedError<A, B> {
    A(A),
    B(B),
}

/// A combined dac array consisting of two [DacArray]s.
/// ports below the cut point will be sent to the "a" array, where ports above or equal
/// to the cut point will be sent to the "b" array.  Note that, the cut point will be
/// subtracted from the port before it is sent to "b".
pub struct CombinedDacArray<A, B, Port> {
    a: A,
    b: B,
    cut_point: Port,
}

impl<A, B, Port> CombinedDacArray<A, B, Port> {
    pub fn new<Word>(a: A, b: B, cut_point: Port) -> CombinedDacArray<A, B, Port>
    where
        Port: Copy + PartialOrd + core::ops::Sub<Output = Port>,
        A: DacArray<Word, Port>,
        B: DacArray<Word, Port>,
    {
        CombinedDacArray { a, b, cut_point }
    }

    pub fn free(self) -> (A, B) {
        (self.a, self.b)
    }
}

impl<A, B, Word, Port> DacArray<Word, Port> for CombinedDacArray<A, B, Port>
where
    Port: Copy + PartialOrd + core::ops::Sub<Output = Port>,
    A: DacArray<Word, Port>,
    B: DacArray<Word, Port>,
{
    type Error = CombinedError<A::Error, B::Error>;
    fn write(&mut self, val: Word, port: Port) -> Result<(), Self::Error> {
        if port < self.cut_point {
            self.a
                .write(val, port)
                .map_err(CombinedError::<A::Error, B::Error>::A)
        } else {
            self.b
                .write(val, port - self.cut_point)
                .map_err(CombinedError::<A::Error, B::Error>::B)
        }
    }
}

#[cfg(test)]
pub mod test_dac_array {
    use crate::dac_array::DacArray;

    #[derive(Default, Debug)]
    pub struct TestDacArray<E> {
        pub next_err: Option<E>,
        pub writes: Vec<(u64, usize)>,
    }

    impl<E> DacArray<u64, usize> for TestDacArray<E>
    where
        E: Clone,
    {
        type Error = E;
        fn write(&mut self, val: u64, port: usize) -> Result<(), Self::Error> {
            if let Some(err) = self.next_err.as_ref() {
                Err(err.clone())
            } else {
                self.writes.push((val, port));
                Ok(())
            }
        }
    }
}

#[cfg(test)]
pub mod combined_dac_tests {
    use crate::dac_array::test_dac_array::TestDacArray;
    use crate::dac_array::{CombinedDacArray, CombinedError, DacArray};
    #[test]
    fn combined_low() {
        let mut combined = CombinedDacArray::new(Default::default(), Default::default(), 7);
        assert_eq!(Ok(()), combined.write(33, 2));
        let (lo, hi): (TestDacArray<u32>, TestDacArray<u32>) = combined.free();
        assert_eq!(vec!((33, 2)), lo.writes);
        assert_eq!(Vec::<(u64, usize)>::new(), hi.writes);
    }

    #[test]
    fn combined_hi() {
        let mut combined = CombinedDacArray::new(Default::default(), Default::default(), 7);
        assert_eq!(Ok(()), combined.write(33, 9));
        let (lo, hi): (TestDacArray<u32>, TestDacArray<u32>) = combined.free();
        assert_eq!(vec!((33, 2)), hi.writes);
        assert_eq!(Vec::<(u64, usize)>::new(), lo.writes);
    }

    #[test]
    fn combined_err_lo() {
        let mut combined = CombinedDacArray::<TestDacArray<u64>, TestDacArray<String>, usize>::new(
            TestDacArray {
                next_err: Some(37),
                writes: Default::default(),
            },
            Default::default(),
            7,
        );
        assert_eq!(Err(CombinedError::A(37)), combined.write(33, 2));
        let (lo, hi) = combined.free();
        assert_eq!(Vec::<(u64, usize)>::new(), hi.writes);
        assert_eq!(Vec::<(u64, usize)>::new(), lo.writes);
    }

    #[test]
    fn combined_err_hi() {
        let mut combined = CombinedDacArray::<TestDacArray<u64>, TestDacArray<String>, usize>::new(
            Default::default(),
            TestDacArray {
                next_err: Some("foo".to_string()),
                writes: Default::default(),
            },
            7,
        );
        assert_eq!(
            Err(CombinedError::B("foo".to_string())),
            combined.write(33, 9)
        );
        let (lo, hi) = combined.free();
        assert_eq!(Vec::<(u64, usize)>::new(), hi.writes);
        assert_eq!(Vec::<(u64, usize)>::new(), lo.writes);
    }
}

/// A [DacArray] consisting only of a single [Dac]
pub struct SingleDacArray<D> {
    dac: D,
}

impl<D> SingleDacArray<D> {
    pub fn new<Word>(dac: D) -> SingleDacArray<D>
    where
        D: Dac<Word>,
    {
        SingleDacArray { dac }
    }

    pub fn free(self) -> D {
        self.dac
    }
}

impl<D, W, P> DacArray<W, P> for SingleDacArray<D>
where
    D: Dac<W>,
    P: Copy + PartialEq + core::ops::Sub<Output = P>,
{
    type Error = D::Error;
    fn write(&mut self, val: W, port: P) -> Result<(), Self::Error> {
        #![allow(clippy::eq_op)]
        assert!(port == port - port);
        self.dac.write(val)
    }
}

#[cfg(test)]
pub mod single_tests {
    use crate::dac::Dac;
    use crate::dac_array::{DacArray, SingleDacArray};

    #[derive(Default, Debug)]
    struct TestDac<E> {
        next_err: Option<E>,
        writes: Vec<u64>,
    }

    impl<E> Dac<u64> for TestDac<E>
    where
        E: Clone,
    {
        type Error = E;

        fn write(&mut self, data: u64) -> Result<(), Self::Error> {
            if let Some(err) = self.next_err.as_ref() {
                Err(err.clone())
            } else {
                self.writes.push(data);
                Ok(())
            }
        }
    }

    #[test]
    fn single_dac_write() {
        let mut array = SingleDacArray::<TestDac<u64>>::new(Default::default());
        assert_eq!(Ok(()), array.write(32, 0));
        let single = array.free();
        assert_eq!(vec!(32), single.writes);
    }

    #[test]
    fn single_dac_error() {
        let mut array = SingleDacArray::new(TestDac::<String> {
            next_err: Some("foo".to_string()),
            writes: Default::default(),
        });
        assert_eq!(Err("foo".to_string()), array.write(32, 0));
        let single = array.free();
        assert_eq!(Vec::<u64>::new(), single.writes);
    }
}

/// A [DacArray] where the ports have been re-arranged according to a map.
pub struct RemappedDacArray<D, P: 'static> {
    dac_array: D,
    map: &'static [P],
}

impl<D, P> RemappedDacArray<D, P> {
    pub fn new<W>(dac_array: D, map: &'static [P]) -> RemappedDacArray<D, P>
    where
        D: DacArray<W, P>,
    {
        RemappedDacArray { dac_array, map }
    }

    pub fn free(self) -> D {
        self.dac_array
    }
}

impl<D, W, P> DacArray<W, P> for RemappedDacArray<D, P>
where
    D: DacArray<W, P>,
    P: Copy + Into<usize>,
{
    type Error = D::Error;
    fn write(&mut self, val: W, port: P) -> Result<(), Self::Error> {
        assert!(port.into() < self.map.len());
        self.dac_array.write(val, self.map[port.into()])
    }
}

#[cfg(test)]
pub mod remapped_tests {
    use crate::dac_array::test_dac_array::TestDacArray;
    use crate::dac_array::{DacArray, RemappedDacArray};

    #[test]
    fn remapping() {
        let mut remapped =
            RemappedDacArray::<TestDacArray<u64>, usize>::new(Default::default(), &[1, 0, 2]);
        assert_eq!(Ok(()), remapped.write(17, 1));
        assert_eq!(Ok(()), remapped.write(18, 2));
        let array = remapped.free();
        assert_eq!(vec![(17, 0), (18, 2)], array.writes);
    }

    #[test]
    fn error() {
        let mut remapped = RemappedDacArray::new(
            TestDacArray::<String> {
                next_err: Some("foo".to_string()),
                writes: Default::default(),
            },
            &[1, 0, 2],
        );
        assert_eq!(Err("foo".to_string()), remapped.write(17, 1));
    }
}
