#![no_std]
#![warn(nonstandard_style, rust_2018_idioms, future_incompatible, clippy::all)]
#![deny(warnings)]

use arrayvec::ArrayVec;

#[allow(unused_imports)]
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct TunedPoint {
    pub nominal: f32,
    pub dac: u16,
}

pub const MAX_NUM_POINTS: usize = 5;

type Points = ArrayVec<TunedPoint, MAX_NUM_POINTS>;

/// A piecewise linear output function meant to connect logical values
/// from the voice logic in `f32` to DAC output values in `u16`.
#[derive(Clone, Debug, PartialEq)]
pub struct TunedOutput {
    points: Points,
}

impl TunedOutput {
    /// Creates a new [TunedOutput]. ***Panics*** if there's not enough room for that
    /// many tuning points!  All points must be finite and no two points may have equal
    /// nominal values.
    pub fn new<'a, I>(i: I) -> TunedOutput
    where
        I: core::iter::IntoIterator<Item = &'a TunedPoint>,
    {
        let mut points = Points::new();
        i.into_iter().for_each(|x| {
            points.push(*x);
        });
        assert!(points.len() > 1);
        points
            .as_mut_slice()
            .sort_unstable_by(|a, b| b.nominal.partial_cmp(&a.nominal).unwrap());
        TunedOutput { points }
    }

    /// Creates a standard linear tuning encompasing the full range of output values
    ///
    /// ```
    /// # use tuning::*;
    /// let x = TunedOutput::default_tuning(0f32..=3f32);
    /// assert_eq!(0, x.output(0.));
    /// assert_eq!(core::u16::MAX, x.output(3.));
    /// ```
    pub fn default_tuning(r: core::ops::RangeInclusive<f32>) -> TunedOutput {
        TunedOutput::new(&[
            TunedPoint {
                nominal: *r.start(),
                dac: 0,
            },
            TunedPoint {
                nominal: *r.end(),
                dac: core::u16::MAX,
            },
        ])
    }

    /// Creates a inverted linear tuning encompasing the full range of output values
    ///
    /// ```
    /// # use tuning::*;
    /// let x = TunedOutput::default_inverted_tuning(0f32..=3f32);
    /// assert_eq!(core::u16::MAX, x.output(0.));
    /// assert_eq!(0, x.output(3.));
    /// ```
    pub fn default_inverted_tuning(r: core::ops::RangeInclusive<f32>) -> TunedOutput {
        TunedOutput::new(&[
            TunedPoint {
                nominal: *r.start(),
                dac: core::u16::MAX,
            },
            TunedPoint {
                nominal: *r.end(),
                dac: 0,
            },
        ])
    }

    /// Output the tuned value for the given nominal value, clipping to the range specified
    /// by the tuning points.
    ///
    /// ```
    /// # use tuning::*;
    /// let x = TunedOutput::new(&[
    ///         TunedPoint{nominal: 0f32, dac: 0},
    ///         TunedPoint{nominal: 1f32, dac: 2},
    ///         TunedPoint{nominal: 2f32, dac: 10},
    ///     ]);
    /// assert_eq!(0, x.output(-1.));
    /// assert_eq!(0, x.output(0.));
    /// assert_eq!(1, x.output(0.5));
    /// assert_eq!(2, x.output(1.));
    /// assert_eq!(10, x.output(2.));
    /// assert_eq!(10, x.output(3.));
    /// ```
    ///
    /// Note that this also works for descending values:
    /// ```
    /// # use tuning::*;
    /// let x = TunedOutput::new(&[
    ///         TunedPoint{nominal: 0f32, dac: 4},
    ///         TunedPoint{nominal: 1f32, dac: 2},
    ///         TunedPoint{nominal: 2f32, dac: 0},
    ///     ]);
    /// assert_eq!(4, x.output(-1.));
    /// assert_eq!(4, x.output(0.));
    /// assert_eq!(3, x.output(0.5));
    /// assert_eq!(2, x.output(1.));
    /// assert_eq!(0, x.output(2.));
    /// assert_eq!(0, x.output(3.));
    /// ```
    ///
    /// The points do not have to be in any particular order:
    /// ```
    /// # use tuning::*;
    /// let x = TunedOutput::new(&[
    ///         TunedPoint{nominal: 1f32, dac: 2},
    ///         TunedPoint{nominal: 0f32, dac: 0},
    ///         TunedPoint{nominal: 2f32, dac: 10},
    ///     ]);
    /// assert_eq!(0, x.output(0.));
    /// assert_eq!(1, x.output(0.5));
    /// assert_eq!(2, x.output(1.));
    /// assert_eq!(10, x.output(2.));
    /// ```
    pub fn output(&self, x: f32) -> u16 {
        for (i, TunedPoint { nominal, dac }) in self.points.iter().enumerate() {
            if x >= *nominal {
                if i == 0 {
                    return *dac;
                } else {
                    let p_nominal = self.points[i - 1].nominal;
                    let p_dac = self.points[i - 1].dac;
                    return libm::roundf(
                        (x - p_nominal) / (nominal - p_nominal)
                            * (f32::from(*dac) - f32::from(p_dac))
                            + f32::from(p_dac),
                    ) as u16;
                }
            }
        }
        self.points[self.points.len() - 1].dac
    }

    pub fn free(self) -> Points {
        self.points
    }
}
