# What is this?

This is a work-in-progress repository containing a partial design for a polyphonic analog synth.  The name will change when the project is closer to complete.

# Copyright notice

poly-mcu
Copyright (C) 2019 Russell McClellan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program in the COPYING file.  If not, see <https://www.gnu.org/licenses/>.

